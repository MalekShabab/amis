from odoo import http
from odoo.http import request

class Overtime(http.Controller):

      @http.route('/create_adsal', type='json', auth='none', csrf=False, cors='*')
      def create_adsal(self, db, login, password, advance, date, reason):
        request.session.logout()
        request.session.authenticate(db, login, password)
        if request.jsonrequest:
         # print ("rec", rec)
         # if rec['state']:
              vals = {
                      "advance":advance,
                      "date":date,
                      "reason":reason
                    #  "state":"draft"
              # 'date_from': '2020-09-07 08:08:05',
              # 'date_to': '2020-09-07 08:08:05'
            }
              new_adsal = request.env['salary.advance'].sudo().create(vals)
        print("New  Advance Salary", new_adsal)
        args = {'success': True, 'message': 'Success', 'ID':new_adsal.id}
        return args
      @http.route('/delete_adsal', type='json', auth='none', csrf=False, cors='*')
      def unlink_adsal(self, db, login, password, num):
        request.session.logout()
        request.session.authenticate(db, login, password)
        if request.jsonrequest:
         # print ("rec", rec)
         # if rec['state']:
              vals = num
                     # "id": num
              # 'date_from': '2020-09-07 08:08:05',
              # 'date_to': '2020-09-07 08:08:05'
          #  }
              #del_off = request.env['hr.leave'].sudo().unlink(vals)
        #      request.env['hr.leave'].unlink(vals)
              adsal = request.env['salary.advance'].search([('id', '=', num)])
              delete = adsal.unlink()
        print("Deleted  Off Is", delete)
        args = {'success': True, 'message': 'Success', 'ID':delete}
        return args
      @http.route('/get_hr_advance', type='json', auth='none', csrf=False, cors='*')
      #def get_overtimes(self):
      def hr_adsalary(self, db, login, password, base_location=None):
          request.session.logout()
          request.session.authenticate(db, login, password)
          advance_rec = request.env['salary.advance'].search([('state', '=', "t_approve")])
          advances = []
          for rec in advance_rec:
              vals = {
                      'employee': rec.employee_id.name,
                      'id': rec.id,
                      'amount':rec.advance,
                      'payment date': rec.date,
                      'reason': rec.reason,
                      'state': rec.state,
                      }
              advances.append(vals)
          data = {'status': 200, 'response': advances, 'message': 'Success'}
          return data
      @http.route('/accept_hr_advance', type='json', auth='none', csrf=False, cors='*')
      def accept_hr_advance(self, db, login, password, num):
        request.session.logout()
        request.session.authenticate(db, login, password)
        if request.jsonrequest:
            vals = num
            newstate = {
                    "state":"approve"
                    }
                     # "id": num
              # 'date_from': '2020-09-07 08:08:05',
              # 'date_to': '2020-09-07 08:08:05'
          #  }
              #del_off = request.env['hr.leave'].sudo().unlink(vals)
        #      request.env['hr.leave'].unlink(vals)
            advance = request.env['salary.advance'].search([('id', '=', vals)])
            write = leave.advance(newstate)
        print("Accept  Overtime By HR", write)
        args = {'success': True, 'message': 'Success', 'ID':write}
        return args
      @http.route('/refuse_hr_advance', type='json', auth='none', csrf=False, cors='*')
      def refuse_hr_advance(self, db, login, password, num):
        request.session.logout()
        request.session.authenticate(db, login, password)
        if request.jsonrequest:
            vals = num
            newstate = {
                    "state":"refused"
                    }
                     # "id": num
              # 'date_from': '2020-09-07 08:08:05',
              # 'date_to': '2020-09-07 08:08:05'
          #  }
              #del_off = request.env['hr.leave'].sudo().unlink(vals)
        #      request.env['hr.leave'].unlink(vals)
            leave = request.env['salary.advance'].search([('id', '=', vals)])
            finish = leave.write(newstate)
        print("RefuseOvertime Is", finish)
        args = {'success': True, 'message': 'Success', 'ID':finish}
        return args
      @http.route('/get_dvance', type='json', auth='none', csrf=False, cors='*')
      #def get_overtimes(self):
      def hr_advance(self, db, login, password, base_location=None):
          request.session.logout()
          request.session.authenticate(db, login, password)
          advance_rec = request.env['salary.advance'].search([])
          advances = []
          for rec in advance_rec:
              vals = {
                      'employee': rec.employee_id.name,
                      'id': rec.id,
                      'amount':rec.advance,
                      'payment date': rec.date,
                      'reason': rec.reason,
                      'state': rec.state,
                      }
              advances.append(vals)
          data = {'status': 200, 'response': advances, 'message': 'Success'}
          return data
