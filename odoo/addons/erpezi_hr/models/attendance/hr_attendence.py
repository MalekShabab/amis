import logging
_logger = logging.getLogger(__name__)

from odoo import api, fields, models
from datetime import date, datetime, time
import pyodbc
from pytz import timezone, UTC
from dateutil import relativedelta

class Attendance(models.Model):

    _inherit = "hr.attendance"
    sql_Server_id_max = fields.Integer(readonly=True)
    sql_Server_check_in_id = fields.Integer(readonly=True)
    upload_time = fields.Date(readonly=True)
    area_name = fields.Char(string='Location Name')
    break_in_2 = fields.Datetime(string="Second Break In")
    break_in_3 = fields.Datetime(string="Third Break In")
    break_out_2 = fields.Datetime(string="Second Break Out")
    break_out_3 = fields.Datetime(string="Third Break Out")
    is_late_check_in = fields.Boolean(compute='_get_is_late_check',
                                              string="Late Check In",
                                              compute_sudo=True, store=True)
    is_early_check_out = fields.Boolean(compute='_get_is_late_check',
                                              string="Early Check Out",
                                              compute_sudo=True, store=True)
    late_in_hours = fields.Float(string="Late In Hours")
    late_price = fields.Float(string="Late Price")
    is_deduction = fields.Boolean(string="Is Deduction")


    def action_deduction(self):
        for each in self:
            each.is_deduction = True

    def action_cancel_deduction(self):
        for each in self:
            each.is_deduction = False

    @api.depends('check_in','check_out')
    def _get_is_late_check(self):
      for each in self:
       if  each.check_in and each.check_out:
        localized_check_in = timezone('UTC').localize(each.check_in).astimezone(timezone(self.env.user.tz))
        localized_check_out = timezone('UTC').localize(each.check_out).astimezone(timezone(self.env.user.tz))
        weekday = localized_check_in.weekday()
        current_shift = each.employee_id.resource_calendar_id.attendance_ids.filtered(lambda a: a.dayofweek == str(weekday))[0]
        each.late_price = 0
        if current_shift:
          _logger.warning("localized_check_in is '%s' current_shift is '%s'"%(localized_check_in,current_shift))
          if (localized_check_in.hour > current_shift.hour_from or
             (localized_check_in.hour == current_shift.hour_from and localized_check_in.minute >= 15)):
              each.is_late_check_in = True
              if each.check_in:
                  localized_check_in_officaly = localized_check_in.replace(minute=0, hour=int(current_shift.hour_from), second=0)
                  each.late_in_hours = (localized_check_in - localized_check_in_officaly).seconds / 3600
                  each.late_price = (each.late_in_hours * each.employee_id.contract_id.gl)
          else:
              each.is_late_check_in = False

          if (localized_check_out.hour < (current_shift.hour_to - 1) or
             (localized_check_out.hour == (current_shift.hour_to - 1) and localized_check_out.minute <= 45)):
              each.is_early_check_out = True
              if each.check_out:
                 localized_check_out_officaly = localized_check_out.replace(minute=0, hour=int(current_shift.hour_to), second=0)
                 each.late_in_hours = (localized_check_out_officaly - localized_check_out).seconds / 3600
                 each.late_price =each.late_price + (each.late_in_hours * each.employee_id.contract_id.gl)
          else:
              each.is_early_check_out = False

        else:
            each.is_late_check_in = False




    def _get_data_from_sql_server(self):
      #  driver = 'SQL Server'
        driver = 'ODBC Driver 17 for SQL Server'
        server = 'asifaims.ddns.net'
        db = 'zkteco_biotime'
        user = 'sameerz09'
        password = 'Test@111'

        last_recored_upload_time = False
        attendances = self.env['hr.attendance'].search([], order='sql_Server_id_max desc')
        if attendances:
            last_recored_upload_time = attendances[0].sql_Server_id_max

        sql_query = """  
    select top 1000  Id,CAST(CheckInPin as int) as Employee_Id ,CheckInTime,CheckOutTime,BreakInTime,BreakOutTime,BreakInTime2,BreakOutTime2,BreakInTime3,BreakOutTime3,Area,CheckInUploadTime,CheckOutUploadTime,BreakInUploadTime,BreakInUploadTime2,BreakInUploadTime3,BreakOutUploadTime,BreakOutUploadTime2,BreakOutUploadTime3 from (

	select * from (
		----------------------------------------------------------------------------
	select * from (
        select * from (	   
              select * from (
                   select  min(Id) as Id, min(checktime) as CheckInTime, CAST(checktime as date) as CheckIn,pin as CheckInPin , min(Id) as CheckInUploadTime , min(area_name) as Area  from [zkteco_biotime].[dbo].[checkinout] group by CAST(checktime as date),checkType,pin having checkType =0
               ) checkin left outer join

        (select  max(checktime) as CheckOutTime, CAST(checktime as date) as CheckOut,pin as CheckOutPin, max(Id) as CheckOutUploadTime  from [zkteco_biotime].[dbo].[checkinout] group by CAST(checktime as date),checkType,pin having checkType =1) Checkout 

        on checkin.CheckIn = Checkout.CheckOut and checkin.CheckInPin = Checkout.CheckOutPin
        ) CheckInOut

         left outer join 


         (
            select * from (
                   select min(checktime) as BreakInTime,CAST(checktime as date) as BreakIn,pin as BreakInPin, min(Id) as BreakInUploadTime  from [zkteco_biotime].[dbo].[checkinout] group by CAST(checktime as date),checkType,pin having checkType =3
               ) BreakIn left outer join

        (select min(checktime) as BreakOutTime, CAST(checktime as date) as BreakOut,pin as BreakOutPin, min(Id) as BreakOutUploadTime from [zkteco_biotime].[dbo].[checkinout] group by CAST(checktime as date),checkType,pin having checkType =4) BreakOut 

        on BreakIn.BreakIn = BreakOut.BreakOut and BreakIn.BreakInPin = BreakOut.BreakOutPin

         ) BreakInOut


         on  BreakInOut.BreakIn = CheckInOut.CheckIn and BreakInOut.BreakInPin = CheckInOut.CheckInPin


		 ) originalbiotime
	   ----------------------------------------------------------------------------


		   left outer join 

         (

		 ---------------------------------------------
            select * from (
                       select * from (
                         select row_number() OVER (Partition By CAST(checktime as date),checkType,pin ORDER BY checktime ) Rnk, checktime as BreakInTime2, CAST(checktime as date) as BreakIn2,pin as BreakInPin2, Id as BreakInUploadTime2   , checkType as BreakIn2CheckType
				            from [zkteco_biotime].[dbo].[checkinout] 
                        ) BreakTime2
				        WHERE  Rnk = 2  and BreakIn2CheckType =3
			  ) BreakIn2 left outer join

        (
		
		select * from (
		select row_number() OVER (Partition By CAST(checktime as date),checkType,pin ORDER BY checktime ) Rnk2, checktime as BreakOutTime2, CAST(checktime as date) as BreakOut2,pin as BreakOutPin2, Id as BreakOutUploadTime2  , checkType as BreakOut2CheckType
		
		from [zkteco_biotime].[dbo].[checkinout] 
		
		) BreakOut2
		 WHERE  Rnk2 = 2  and BreakOut2CheckType =4
		) 
		
		BreakOut2

        on BreakIn2.BreakIn2 = BreakOut2.BreakOut2 and BreakIn2.BreakInPin2 = BreakOut2.BreakOutPin2

		---------------------------------------

         ) BreakInOut2
         on  BreakInOut2.BreakIn2 = originalbiotime.CheckIn and BreakInOut2.BreakInPin2 = originalbiotime.CheckInPin
		 ) originalbiotime2


		 
		   left outer join 

         (

		 ---------------------------------------------

            select * from (
                       select * from (
                         select row_number() OVER (Partition By CAST(checktime as date),checkType,pin ORDER BY checktime ) Rnk3, checktime as BreakInTime3, CAST(checktime as date) as BreakIn3,pin as BreakInPin3, Id as BreakInUploadTime3   , checkType as BreakIn3CheckType
				            from [zkteco_biotime].[dbo].[checkinout] 
                        ) BreakTime3
				        WHERE  Rnk3 = 3  and BreakIn3CheckType =3
			  ) BreakIn3 left outer join

        (
		
		select * from (
		select row_number() OVER (Partition By CAST(checktime as date),checkType,pin ORDER BY checktime ) Rnk32, checktime as BreakOutTime3, CAST(checktime as date) as BreakOut3,pin as BreakOutPin3, Id as BreakOutUploadTime3  , checkType as BreakOut3CheckType
		
		from [zkteco_biotime].[dbo].[checkinout] 
		
		) BreakOut3
		 WHERE  Rnk32 = 3  and BreakOut3CheckType =4
		) 
		
		BreakOut3

        on BreakIn3.BreakIn3 = BreakOut3.BreakOut3 and BreakIn3.BreakInPin3 = BreakOut3.BreakOutPin3

	     ) BreakInOut3
         on  BreakInOut3.BreakIn3 = originalbiotime2.CheckIn and BreakInOut3.BreakInPin3 = originalbiotime2.CheckInPin
		 ) originalbiotime2
        """

        if last_recored_upload_time:
            sql_query += "where CheckInUploadTime > " + str(last_recored_upload_time) + " or CheckOutUploadTime > " + str(last_recored_upload_time) +" or BreakInUploadTime > " + str(last_recored_upload_time) +" or BreakOutUploadTime > " + str(last_recored_upload_time) +" or BreakInUploadTime2 > " + str(last_recored_upload_time)+" or BreakInUploadTime3 > " + str(last_recored_upload_time)+" or BreakOutUploadTime2 > " + str(last_recored_upload_time)+" or BreakOutUploadTime3 > " + str(last_recored_upload_time)

        sql_query+=" order by CheckInUploadTime,CheckOutUploadTime,BreakInUploadTime,BreakOutUploadTime,BreakInUploadTime2,BreakOutUploadTime2,BreakInUploadTime3,BreakOutUploadTime3"
#         conn = pyodbc.connect('driver={%s};server=%s;database=%s;uid=%s;pwd=%s' % (driver, server, db, user, password))
#         cursor = conn.cursor()
#         return cursor.execute(sql_query)
        return False

    def _update_company_last_attendants_schedule_work(self, total):
        today = datetime.today()
        companies = self.env['res.company'].search([])
        for company in companies:
            if company.id == self.env.company.id:
                company.last_attendants_schedule_work_date = today
                company.last_attendants_schedule_work_total = total



    @api.model
    def _fetch_attendance_from_sql_server(self):

        rowsNumber=0
        cursor = self._get_data_from_sql_server()
        #while(cursor):
        for row in cursor:
                employee_id = self.env['hr.employee'].search([('id', '=', row.Employee_Id)])

                if employee_id:
                    checkInTime = row.CheckInTime
                    checkOutTime = row.CheckOutTime
                    breakInTime = row.BreakInTime
                    breakOutTime = row.BreakOutTime
                    breakInTime2 = row.BreakInTime2
                    breakOutTime2 = row.BreakOutTime2
                    breakInTime3 = row.BreakInTime3
                    breakOutTime3 = row.BreakOutTime3
                    max_upload_time = row.CheckInUploadTime
                    check_in_id = row.CheckInUploadTime
                    if row.CheckInTime and row.CheckOutTime and row.CheckInTime > row.CheckOutTime:
                        checkInTime = row.CheckOutTime
                        checkOutTime = row.CheckInTime
                    if row.BreakInTime and row.BreakOutTime and row.BreakInTime > row.BreakOutTime:
                        breakInTime = row.BreakOutTime
                        breakOutTime = row.BreakInTime
                    if row.BreakInTime2 and row.BreakOutTime2 and row.BreakInTime2 > row.BreakOutTime2:
                        breakInTime2 = row.BreakOutTime2
                        breakOutTime2 = row.BreakInTime2
                    if row.BreakInTime3 and row.BreakOutTime3 and row.BreakInTime3 > row.BreakOutTime3:
                        breakInTime3 = row.BreakOutTime3
                        breakOutTime3 = row.BreakInTime3
                    if row.CheckInTime and not row.CheckOutTime:
                        checkOutTime = row.CheckInTime

                    if row.CheckOutUploadTime:
                        if not max_upload_time or (max_upload_time and row.CheckOutUploadTime > max_upload_time):
                            max_upload_time = row.CheckOutUploadTime

                    if row.BreakInUploadTime:
                        if not max_upload_time or (max_upload_time and row.BreakInUploadTime > max_upload_time):
                            max_upload_time = row.BreakInUploadTime

                    if row.BreakOutUploadTime:
                        if not max_upload_time or (max_upload_time and row.BreakOutUploadTime > max_upload_time):
                            max_upload_time = row.BreakOutUploadTime

                    if row.BreakInUploadTime2:
                        if not max_upload_time or (max_upload_time and row.BreakInUploadTime2 > max_upload_time):
                            max_upload_time = row.BreakInUploadTime2

                    if row.BreakOutUploadTime2:
                        if not max_upload_time or (max_upload_time and row.BreakOutUploadTime2 > max_upload_time):
                            max_upload_time = row.BreakOutUploadTime2

                    if row.BreakInUploadTime3:
                        if not max_upload_time or (max_upload_time and row.BreakInUploadTime3 > max_upload_time):
                            max_upload_time = row.BreakInUploadTime3

                    if row.BreakOutUploadTime3:
                        if not max_upload_time or (max_upload_time and row.BreakOutUploadTime3 > max_upload_time):
                            max_upload_time = row.BreakOutUploadTime3

                    attendance = self.env['hr.attendance'].search([('sql_Server_check_in_id', '=', check_in_id)])
                    if attendance:
                       attendance.unlink()

                    self.env["hr.attendance"].create(
                        {
                            "employee_id": row.Employee_Id,
                            "check_in": checkInTime,
                            "check_out": checkOutTime,
                            "break_in": breakInTime,
                            "break_out": breakOutTime,
                            "sql_Server_id_max": max_upload_time,
                            "sql_Server_check_in_id": check_in_id,
                            "break_in_2": breakInTime2,
                            "break_in_3": breakInTime3,
                            "break_out_2": breakOutTime2,
                            "break_out_3": breakOutTime3,
                            "area_name": row.Area
                        }
                    )
                    rowsNumber+=1
            #self.env.cr.commit()
            #cursor = self._get_data_from_sql_server()
        self._update_company_last_attendants_schedule_work(rowsNumber)



#new by labib#