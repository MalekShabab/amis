from odoo import http
from odoo.http import request
from pytz import timezone, UTC
from datetime import timedelta, datetime

class Overtime(http.Controller):
    @http.route('/get_overtime', type='json', auth='none', csrf=False, cors='*')
  #  def get_overtimes(self):
    def overtime(self, db, login, password, base_location=None):
          request.session.logout()
          request.session.authenticate(db, login, password)
          overtimes_rec = request.env['hr.overtime'].search([('employee_id.id','=',login)])
          overtimes = []
          DateFormat = "%Y-%m-%d %H:%M:%S";
          for rec in overtimes_rec:
              dfrom =  datetime.strptime(str(rec.date_from), DateFormat) + timedelta(hours=3)
              dto =  datetime.strptime(str(rec.date_to), DateFormat) + timedelta(hours=3)
              vals = {
                      'employee': rec.employee_id.name,
                      'id': rec.id,
                      'from': dfrom,
                      'to': dto,
                      'duration': rec.days_no_tmp,
                      'state': rec.state,
                      'reason':rec.reason,
                      'device_info': rec.device_info,
                      'device_type':rec.device_type
                      }
              overtimes.append(vals)
          data = {'status': 200, 'response': overtimes, 'message': 'Success'}
          return data
    @http.route('/create_overtime', type='json', auth='none', csrf=False, cors='*')
    def create_overtime(self, db, login, password, state, date_from, date_to, reason, device_info, device_type):
        request.session.logout()
        request.session.authenticate(db, login, password)
        if request.jsonrequest:
              DateFormat = "%Y-%m-%d %H:%M:%S";
              check_from = datetime.strptime(date_from, DateFormat);
              ovfrom = check_from + timedelta(hours=-3);
              check_to = datetime.strptime(date_to, DateFormat);
              ovto = check_to + timedelta(hours=-3);



              vals = {
                      "state":state,
                      "date_from":ovfrom,
                      "date_to":ovto,
                      'reason':reason,
                      'device_info':device_info,
                      'device_type':device_type

            }
              new_overtime = request.env['hr.overtime'].sudo().create(vals)
        print("New Overtime Is", new_overtime)
        args = {'success': True, 'message': 'Success', 'ID':new_overtime.id}
        return args
  
    @http.route('/delete_overtime', type='json', auth='none', csrf=False, cors='*')
    def unlink_overtime(self, db, login, password, num):
        request.session.logout()
        request.session.authenticate(db, login, password)
        if request.jsonrequest:
         # print ("rec", rec)
         # if rec['state']:
              vals = num
                     # "id": num

              # 'date_from': '2020-09-07 08:08:05',
              # 'date_to': '2020-09-07 08:08:05'
          #  }
              #del_off = request.env['hr.leave'].sudo().unlink(vals)
        #      request.env['hr.leave'].unlink(vals)



              overtime = request.env['hr.overtime'].search([('id', '=', num)])
              delete = overtime.unlink()
        print("Deleted  Off Is", delete)
        args = {'success': True, 'message': 'Success', 'ID':delete}
        return args
    @http.route('/update_overtime', type='json', auth='none', csrf=False, cors='*')
    def update_overtime(self, db, login, password, num):
        request.session.logout()
        request.session.authenticate(db, login, password)
        if request.jsonrequest:
            vals = num
            nstate = {
                    "state":"s_approve"
                    }

            leave = request.env['hr.overtime'].search([('id', '=', vals)])
            write = leave.write(nstate)
        print("Deleted  Off Is", write)
        args = {'success': True, 'message': 'Success', 'ID':write}
        return args
    @http.route('/refuse_overtime', type='json', auth='none', csrf=False, cors='*')
    def refuse_overtime(self, db, login, password, num):
        request.session.logout()
        request.session.authenticate(db, login, password)
        if request.jsonrequest:
            vals = num
            newstate = {
                    "state":"refused"
                    }
                     # "id": num
              # 'date_from': '2020-09-07 08:08:05',
              # 'date_to': '2020-09-07 08:08:05'
          #  }
              #del_off = request.env['hr.leave'].sudo().unlink(vals)
        #      request.env['hr.leave'].unlink(vals)
            leave = request.env['hr.overtime'].search([('id', '=', vals)])
            finish = leave.write(newstate)
        print("RefuseOvertime Is", finish)
        args = {'success': True, 'message': 'Success', 'ID':finish}
        return args
    @http.route('/get_login_details', type='json', auth='none', csrf=False, cors='*')
    def off(self, db, login, password, base_location=None):
        request.session.logout()
        request.session.authenticate(db, login, password)
        login_rec = request.env['login.detail'].search_count([('create_uid.id','=',login)])
        data = {'status': 200, 'login counts': login_rec, 'message': 'Success'}
        return data
    @http.route('/change_pass', type='json', auth="none", csrf=False, cors='*')
    def change_password(self, old_pwd, db, login, password,  new_password, base_location=None):
            request.session.logout()
            request.session.authenticate(db, login, password)
            if request.env['res.users'].change_password(old_pwd, new_password):
                return {'new_password':new_password}
    @http.route('/get_overtime_manager', type='json', auth='none', csrf=False, cors='*')
    def overtime_manager_group(self, db, login, password, base_location=None):
        request.session.logout()
        request.session.authenticate(db, login, password)
        flag = request.env['res.users'].search([('id', '=', login)]).has_group('ohrms_overtime.group_ohrms_overtime_manager')
        return flag
    @http.route('/managedovertime', type='json', auth='none', csrf=False, cors='*')
    def managedovertime(self, db, login, password, base_location=None):
        request.session.logout()
        request.session.authenticate(db, login, password)
        overtime_rec = request.env['hr.overtime'].search([('employee_id.leave_manager_id', '=', login), ('state', '=', "f_approve")])
        overtimes = []
        for rec in overtime_rec:
              vals = {
                      'id': rec.id,
                      'name': rec.employee_id.name,
                      'employee_id': rec.employee_id.id,
                      'from': rec.date_from,
                      'to': rec.date_to,
                      'duration': rec.days_no_tmp,
                      'reason': rec.reason,
                      'state':rec.state,
                      }
              overtimes.append(vals)
        data = {'status': 200, 'response': overtimes, 'message': 'Success'}
        return data
    @http.route('/update_lang_ar', type='json', auth='none', csrf=False, cors='*')
    def update_lang_ar(self, db, login, password, num):
        request.session.logout()
        request.session.authenticate(db, login, password)
        if request.jsonrequest:
            vals = num
            nstate = {
                    "lang":"ar_SY"
                    }
            ser = request.env['res.partner'].search([('id', '=', vals)])
            write = ser.write(nstate)
        print("Language Is", write)
        args = {'success': True, 'message': 'Success', 'ID':write}
        return args
    @http.route('/get_lan', type='json', auth='none', csrf=False, cors='*')
  #  def get_lang(self):
    def lan(self, db, login, password, base_location=None):
          request.session.logout()
          request.session.authenticate(db, login, password)
          lan_rec = request.env['res.partner'].search([('id','=',login)])
          lans = []
          for rec in lan_rec:
              vals = {
                      'lang': rec.lang
                      }
              lans.append(vals)
          data = {'status': 200, 'response': lans, 'message': 'Success'}
          return data

    @http.route('/update_lang_en', type='json', auth='none', csrf=False, cors='*')
    def update_lang_en(self, db, login, password, num):
        request.session.logout()
        request.session.authenticate(db, login, password)
        if request.jsonrequest:
            vals = num
            nstate = {
                    "lang":"en_US"
                    }
            ser = request.env['res.partner'].search([('id', '=', vals)])
            write = ser.write(nstate)
        print("Language Is", write)
        args = {'success': True, 'message': 'Success', 'ID':write}
        return args
    @http.route('/get_hr_overtime', type='json', auth='none', csrf=False, cors='*')
    def hr_overtime(self, db, login, password, base_location=None):
        request.session.logout()
        request.session.authenticate(db, login, password)
        overtime_rec = request.env['hr.overtime'].search([('state', '=', "t_approve")])
        overtimes = []
        for rec in overtime_rec:
              vals = {
                      'id': rec.id,
                      'name': rec.employee_id.name,
                      'employee_id':rec.employee_id.id,
                      'type': rec.holiday_status_id.name,
                      'from': rec.request_date_from,
                      'to': rec.request_date_to,
                      'days': rec.number_of_days,
                      'state':rec.state,
                      'reason':rec.reason,
                      'device_info':rec.device_info,
                      'device_type':recrdevice_type,
                      }
              overtimes.append(vals)
        data = {'status': 200, 'response': overtimes, 'message': 'Success'}
        return data
    @http.route('/accept_hr_overtime', type='json', auth='none', csrf=False, cors='*')
    def accept_hr_overtime(self, db, login, password, num):
        request.session.logout()
        request.session.authenticate(db, login, password)
        if request.jsonrequest:
            vals = num
            newstate = {
                    "state":"approved"
                    }
                     # "id": num
              # 'date_from': '2020-09-07 08:08:05',
              # 'date_to': '2020-09-07 08:08:05'
          #  }
              #del_off = request.env['hr.leave'].sudo().unlink(vals)
        #      request.env['hr.leave'].unlink(vals)
            leave = request.env['hr.overtime'].search([('id', '=', vals)])
            write = leave.write(newstate)
        print("Accept  Overtime By HR", write)
        args = {'success': True, 'message': 'Success', 'ID':write}
        return args
    @http.route('/get_hr_manager', type='json', auth='none', csrf=False, cors='*')
    def hr_manager_group(self, db, login, password, base_location=None):
        request.session.logout()
        request.session.authenticate(db, login, password)
        flag = request.env['res.users'].search([('id', '=', login)]).has_group('hr_holidays.group_hr_holidays_manager')
        return flag
    @http.route('/get_finance_manager', type='json', auth='none', csrf=False, cors='*')
    def hr_finance_group(self, db, login, password, base_location=None):
        request.session.logout()
        request.session.authenticate(db, login, password)
        flag = request.env['res.users'].search([('id', '=', login)]).has_group('ohrms_overtime.group_ohrms_overtime_finance_manager')
        return flag
    @http.route('/get_finance_overtime', type='json', auth='none', csrf=False, cors='*')
    def finance_overtime(self, db, login, password, base_location=None):
        request.session.logout()
        request.session.authenticate(db, login, password)
        overtime_rec = request.env['hr.overtime'].search([('state', '=', "s_approve")])
        overtimes = []
        for rec in overtime_rec:
              vals = {
                      'id': rec.id,
                      'name': rec.employee_id.name,
                      'employee_id:': rec.employee_id.id,
                      'type': rec.holiday_status_id.name,
                      'from': rec.request_date_from,
                      'to': rec.request_date_to,
                      'days': rec.number_of_days,
                      'state':rec.state,
                      'reason':rec.reason,
                      'device_info':rec.device_info,
                      'device_type':recrdevice_type,
                      }
              overtimes.append(vals)
        data = {'status': 200, 'response': overtimes, 'message': 'Success'}
        return data
    @http.route('/accept_finacne_overtime', type='json', auth='none', csrf=False, cors='*')
    def accept_finance_overtime(self, db, login, password, num):
        request.session.logout()
        request.session.authenticate(db, login, password)
        if request.jsonrequest:
            vals = num
            newstate = {
                    "state":"approved"
                    }
                     # "id": num
              # 'date_from': '2020-09-07 08:08:05',
              # 'date_to': '2020-09-07 08:08:05'
          #  }
              #del_off = request.env['hr.leave'].sudo().unlink(vals)
        #      request.env['hr.leave'].unlink(vals)
            leave = request.env['hr.overtime'].search([('id', '=', vals)])
            write = leave.write(newstate)
        print("Accept  Overtime By HR", write)
        args = {'success': True, 'message': 'Success', 'ID':write}
        return args
    @http.route('/create_maintenance', type='json', auth='none', csrf=False, cors='*')
    def create_maintenance(self, db, login, password, name, types, description, priority, documents, document_name):
        request.session.logout()
        request.session.authenticate(db, login, password)
        if request.jsonrequest:
         # print ("rec", rec)
         # if rec['state']:
              vals = {
                      "equipment_id":types,
                      "name":name,
                      "description":description,
                      "priority":priority,
                      "documents":documents,
                      "document_name":document_name

              # 'date_from': '2020-09-07 08:08:05',
              # 'date_to': '2020-09-07 08:08:05'
            }
              new_request = request.env['maintenance.request'].sudo().create(vals)
        print("New Mantenace Request Is", new_request)
        args = {'success': True, 'message': 'Success', 'ID':new_request.id}
        return args
    @http.route('/get_mantenance', type='json', auth='none', csrf=False, cors='*')
    def get_mantenance(self, db, login, password, base_location=None):
        request.session.logout()
        request.session.authenticate(db, login, password)
        request_rec = request.env['maintenance.request'].search([('create_uid', '=', login)])
        requests = []
        for rec in request_rec:
              vals = {
                      'id': rec.id,
                      'equipment_id':rec.equipment_id.name,
                #      'owner_user_id':rec.owner_user_id.id,
                      'employee_id':rec.employee_id.name,
                      'name': rec.name,
                      'description': rec.description,
                      'priority': rec.priority,
                      'state': rec.stage_id.name,
                      'response':rec.response
                      }
              requests.append(vals)
        data = {'status': 200, 'response': requests, 'message': 'Success'}
        return data
    @http.route('/create_attendance', type='json', auth='none', csrf=False, cors='*')
    def create_attendance(self, db, login, password, check_in, location_name, latitude, longitude, os_name):
        request.session.logout()
        request.session.authenticate(db, login, password)
        if request.jsonrequest:
         # print ("rec", rec)
         # if rec['state']:

              DateFormat = "%Y-%m-%d %H:%M:%S";
              check_inVar = datetime.strptime(check_in, DateFormat);
              resTime = check_inVar + timedelta(hours=-3);
             # check_outVar = datetime.strptime(check_out, DateFormat);
             # outTime = check_outVar + timedelta(hours=-3);
              vals = {
                      "check_in":resTime,
                      #"check_out":outTime,
                      "location_name":location_name,
                      "latitude":latitude,
                      "longitude":longitude,
                      'os_name':os_name
            
              # 'date_from': '2020-09-07 08:08:05',
              # 'date_to': '2020-09-07 08:08:05'
            }
              new_attendance = request.env['hr.attendance'].sudo().create(vals)
        print("New Attendance Is", new_attendance)
        args = {'success': True, 'message': 'Success', 'ID':new_attendance.id}
        return args
    @http.route('/create_attendance_out', type='json', auth='none', csrf=False, cors='*')
    def create_attendance_out(self, db, login, password, check_out, location_name, latitude, longitude, os_name):
        request.session.logout()
        request.session.authenticate(db, login, password)
       # checkout_rec = request.env['hr.attendance'].search([('check_out', '=', False)])
    #    if checkout_rec:
        checkout_rec = request.env['hr.attendance'].search([('check_out', '=', False), ('create_uid', '=', login)])
        if request.jsonrequest:
         # print ("rec", rec)
         # if rec['state']:
            DateFormat = "%Y-%m-%d %H:%M:%S";
            check_outVar = datetime.strptime(check_out, DateFormat);
            outTime = check_outVar + timedelta(hours=-3);
            vals = {
                      "check_out":outTime,
                      "location_name_out":location_name,
                      "latitude_out":latitude,
                      "longitude_out":longitude,
                      'os_name_out':os_name
              # 'date_from': '2020-09-07 08:08:05',
              # 'date_to': '2020-09-07 08:08:05'
                     }
            new_attendance = request.env['hr.attendance'].search([('check_out', '=', False), ('create_uid', '=', login)],  limit=1, order='id desc')
            for attendance in new_attendance:
               attendance.write({'check_out':outTime,
                               'location_name_out':location_name,
                               'latitude_out':latitude,
                               'longitude_out':longitude,
                               'os_name_out':os_name})
    
        #return new_attendance
        print("New Attendance Is", new_attendance)
        args = {'success': True, 'message': 'Success', 'ID':new_attendance.id}
        return args
    @http.route('/create_break_in', type='json', auth='none', csrf=False, cors='*')
    def create_break_in(self, db, login, password, break_in, location_name, latitude, longitude, os_name):
        request.session.logout()
        request.session.authenticate(db, login, password)
        checkout_rec = request.env['hr.attendance'].search([('create_uid', '=', login)], limit=1, order='id desc')
        if checkout_rec:
          DateFormat = "%Y-%m-%d %H:%M:%S";
          break_inVar = datetime.strptime(break_in, DateFormat);
          breakIn = break_inVar + timedelta(hours=-3);  
            
          if request.jsonrequest:
         # print ("rec", rec)
         # if rec['state']:
            vals = {
                      "break_in":breakIn,
                      "location_name_break_in":location_name,
                      "latitude_break_in":latitude,
                      "longitude_break_in":longitude,
                      'os_name__break_in':os_name
              # 'date_from': '2020-09-07 08:08:05',
              # 'date_to': '2020-09-07 08:08:05'
                     }
            new_attendance = request.env['hr.attendance'].search([('check_out', '=', False), ('create_uid', '=', login)], limit=1, order='id desc')
            for attendance in new_attendance:
               attendance.write({'break_in':breakIn,
                               'location_name_break_in':location_name,
                               'latitude_break_in':latitude,
                               'longitude_break_in':longitude,
                               'os_name_break_in':os_name})
       #return new_attendance
        print("New Attendance Is", new_attendance)
        args = {'success': True, 'message': 'Success', 'ID':new_attendance.id}
        return args
    @http.route('/create_break_out', type='json', auth='none', csrf=False, cors='*')
    def create_break_out(self, db, login, password, break_out, location_name, latitude, longitude, os_name):
        request.session.logout()
        request.session.authenticate(db, login, password)
       # checkout_rec = request.env['hr.attendance'].search([('check_out', '=', False)])
    #    if checkout_rec:

        if request.jsonrequest:
            DateFormat = "%Y-%m-%d %H:%M:%S";
            break_outVar = datetime.strptime(break_out, DateFormat);
            breakOut = break_outVar + timedelta(hours=-3);
         # print ("rec", rec)
         # if rec['state']:
            vals = {
                      "break_out":break_out,
                      "location_name_break_out":location_name,
                      "latitude_break_out":latitude,
                      "longitude_break_out":longitude,
                      'os_name_break_out':os_name
              # 'date_from': '2020-09-07 08:08:05',
              # 'date_to': '2020-09-07 08:08:05'
                     }
            new_attendance = request.env['hr.attendance'].search([('check_out', '=', False), ('create_uid', '=', login)], limit=1, order='id desc')
            for attendance in new_attendance:
               attendance.write({'break_out':break_out,
                               'location_name_break_out':location_name,
                               'latitude_break_out':latitude,
                               'longitude_break_out':longitude,
                               'os_name_break_out':os_name})
        #return new_attendance
        print("New Attendance Is", new_attendance)
        args = {'success': True, 'message': 'Success', 'ID':new_attendance.id}
        return args
    @http.route('/get_myattendance', type='json', auth='none', csrf=False, cors='*')
    def get_myattendance(self, db, login, password, base_location=None):
        request.session.logout()
        request.session.authenticate(db, login, password)
        request_rec = request.env['hr.attendance'].search([('create_uid', '=', login)])
        requests = []
        DateFormat = "%Y-%m-%d %H:%M:%S";
        for rec in request_rec:
#              check_in = null;              
#             if rec.check_in:
#	        check_in = datetime.strptime(str(rec.check_in), DateFormat) + timedelta(hours=3)
#	      if rec.check_out:
#			check_out =  datetime.strptime(str(rec.check_out), DateFormat) + timedelta(hours=3)
#	      else:
#			check_out = rec.check_out


              vals = {
                      'check_in': datetime.strptime(str(rec.check_in), DateFormat) + timedelta(hours=3),
                      'check_out': datetime.strptime(str(rec.check_out), DateFormat) + timedelta(hours=3),
                      'break_in': rec.break_in,
                      'break_out': rec.break_out,
                      }
              requests.append(vals)
        data = {'status': 200, 'response': requests, 'message': 'Success'}
        return data
    @http.route('/web/session/authenticate', type='json', auth="none", csrf=False, cors='*')
    def authenticate(self, db, login, password, base_location=None):
        request.session.authenticate(db, login, password)
        return request.env['ir.http'].session_info()
