from odoo import models, api, fields


class PayslipLoanAmount(models.Model):
    _inherit = 'hr.payslip'
    loan_ids = fields.Many2many('hr.loan.line')


    @api.model
    def get_inputs(self, contracts, date_from, date_to,employee_id=False,leave_days_number=0,is_resignation=False):
        """
        function used for writing overtime record in payslip
        input tree.
        """
        res = super(PayslipLoanAmount, self).get_inputs(contracts, date_from, date_to,employee_id,leave_days_number)
        if not is_resignation or is_resignation == False:
            contract = contracts

            line_id = self.env['hr.loan.line'].search([('employee_id', '=', employee_id),
                                                       ('state', '=', 'approve'),
                                                       ('loan_id', '!=', []),
                                                       ('date', '>=', date_from),
                                                       ('date', '<=', date_to),
                                                       ('paid', '=', False)])

            hrs_amount = line_id.mapped('amount')
            cash_amount = sum(hrs_amount)
            if line_id:
                self.loan_ids = line_id
                input_data = {
                    'name': 'Loan',
                    'code': 'LN100',
                    'amount': cash_amount,
                    'contract_id': contract.id,
                }
                res.append(input_data)

        return res

    def action_payslip_done(self):
        if not self.is_resignation or self.is_resignation == False:
            self.loan_ids = self.env['hr.loan.line'].search([('employee_id', '=', self.employee_id.id),
                                                             ('state', '=', 'approve'),
                                                             ('loan_id', '!=', []),
                                                             ('date', '>=', self.date_from),
                                                             ('date', '<=', self.date_to),
                                                             ('paid', '=', False)])
            if self.loan_ids:
                for loan_line in self.loan_ids:
                    if loan_line:
                        loan_line.paid = True
                        loan_line.loan_id._compute_loan_amount()

        return super(PayslipLoanAmount, self).action_payslip_done()


