from odoo import models, api, fields,_
from odoo.exceptions import UserError, ValidationError
from datetime import date, datetime, time
from datetime import timedelta

class PayslipUnpaidLeave(models.Model):
    _inherit = 'hr.payslip'

    unpaid_ids = fields.Many2many('hr.leave')

    @api.model
    def get_inputs(self, contracts, date_from, date_to,employee_id=False,leave_days_number=0,is_resignation=False):
        day_from = datetime.combine(fields.Date.from_string(date_from), time.min)
        day_to = datetime.combine(fields.Date.from_string(date_to), time.max)
        month_end_day = 30
        if is_resignation == True:
            month_end_day = date_to.day
        res = super(PayslipUnpaidLeave, self).get_inputs(contracts, date_from, date_to,employee_id)

        unpaid_id = self.getUnpaid_id(contracts,date_from,date_to)
        self.unpaid_ids = unpaid_id
        holiday_id = 0
        leave_number_of_days = 0
        for unpaid in unpaid_id:
            holiday = None


            if unpaid.employee_id.id == contracts.employee_id.id and unpaid.payslip_status == False and unpaid.holiday_status_code in ['AV', 'LL', 'EO', 'UV', 'A','UL'] and unpaid.date_from <= day_to and unpaid.date_to >= day_from and unpaid.state == 'validate':
                holiday = unpaid
                date_from_timedelta = holiday.date_from + timedelta(hours=3)
                date_to_timedelta = holiday.date_to + timedelta(hours=3)

                if holiday.id != holiday_id:
                    leave_number_of_days = 0
            if holiday and holiday != False and ((holiday.holiday_status_id.code != 'AV' or holiday.financial_clearance) or holiday.holiday_status_id.code in [
                                                     'LL', 'EO', 'UV', 'A', 'UL']) and not holiday.payslip_status:

                if (holiday.id != holiday_id or holiday.holiday_status_id.code == 'UL' or holiday.holiday_status_id.code == 'EO' or holiday.holiday_status_id.code == 'LL'):
                    holiday_id = holiday.id
                    if (holiday.holiday_status_id.code == 'AV' and holiday.financial_clearance) or holiday.holiday_status_id.code == 'UV' or holiday.holiday_status_id.code == 'A' or holiday.holiday_status_id.code == 'UL' or holiday.holiday_status_id.code == 'EO' or holiday.holiday_status_id.code == 'LL':
                        # all month is off
                        if date_from_timedelta <= day_from and date_to_timedelta >= day_to:
                            leave_number_of_days += month_end_day

                        elif date_from_timedelta < day_from and date_to_timedelta < day_to and date_to_timedelta >= day_from:
                            leave_number_of_days += month_end_day - (day_to.replace(day=month_end_day) - date_to_timedelta).days
                        ##
                        elif holiday.holiday_status_id.code == 'AV' and holiday.financial_clearance and date_from_timedelta >= day_from and date_to_timedelta <= day_to:
                            leave_number_of_days += month_end_day - (day_to.replace(day=month_end_day) - date_to_timedelta).days

                        elif holiday.holiday_status_id.code == 'AV' and holiday.financial_clearance and date_from_timedelta > day_from and date_from_timedelta <= day_to and date_to_timedelta > day_to:
                            leave_number_of_days += month_end_day

                        elif date_from_timedelta >= day_from and date_to_timedelta <= day_to:
                            leave_number_of_days += holiday.number_of_days

                        elif date_from_timedelta > day_from and date_from_timedelta <= day_to and date_to_timedelta > day_to:
                            leave_number_of_days += month_end_day - (date_from_timedelta  - day_to.replace(day=1, hour=0, minute=0, second=0, microsecond=0)).days - 1
                        else:
                            leave_number_of_days += holiday.number_of_days
                    else:
                        leave_number_of_days += 0

            if unpaid.holiday_status_id.code in ['AV', 'LL', 'EO', 'UV', 'A', 'UL']:
                if unpaid.holiday_status_id.code in ['UV', 'UL', 'A']:
                    hrs_amount = leave_number_of_days * contracts.uday
                if unpaid.holiday_status_id.code in ['LL', 'EO']:
                    hrs_amount = leave_number_of_days * contracts.gl
                if unpaid.holiday_status_id.code == 'AV':
                    hrs_amount = leave_number_of_days * contracts.lday

                cash_amount = hrs_amount
                if unpaid and (unpaid.holiday_status_id.code != 'AV' or unpaid.financial_clearance):
                    input_data = {
                        'name': unpaid.holiday_status_id.name,
                        'code': unpaid.holiday_status_id.code,
                        'amount': cash_amount,
                        'contract_id': contracts.id,
                    }
                    dup_val = list(filter(lambda input: input['code'] == input_data['code'], res))
                    if dup_val:
                        dup_val[0]['amount'] = dup_val[0]['amount'] + input_data['amount']
                    else:
                        res.append(input_data)

        return res

    def getUnpaid_id(self, contracts, date_from, date_to):

        unpaid_id = self.env['hr.leave'].search(
            [
             ('employee_id', '=', contracts.employee_id.id),
             ('contract_id', '=', contracts.id),
             ('holiday_status_code', 'in', ['AV','LL','EO','UV','A','UL']),
             ('state', '=', 'validate'),
             ('payslip_status', '=', False),
             ('date_to', '>=', date_from),
             ('date_from', '<=', date_to)
             ]
        )

        return unpaid_id
    # def getUnpaid_id(self, contracts, date_from, date_to):
    #
    #     unpaid_id = self.env['hr.leave'].search(
    #         ['&',
    #          ('employee_id', '=', contracts.employee_id.id),
    #          '&',
    #          ('contract_id', '=', contracts.id),
    #          '&',
    #          ('state', '=', 'validate'),
    #          '&',
    #          ('payslip_status', '=', False),
    #          '|',
    #          '&',
    #          ('date_from', '<=', date_to),
    #          ('date_to', '>=', date_to),
    #          '|',
    #          '&',
    #          ('date_from', '<=', date_from),
    #          ('date_to', '>=', date_from),
    #          '&',
    #          ('date_from', '>=', date_from),
    #          ('date_to', '<=', date_to)
    #          ]
    #     )
    #
    #     return unpaid_id


    def check_is_record_exist(self,payslip):
        payslip_rec = payslip.env['hr.payslip'].search(
            ['&',
             ('employee_id', '=', payslip.employee_id.id),
             '&',
             ('contract_id', '=', payslip.contract_id.id),
             '&',
             ('id', '!=', payslip.id),
             '&',
             ('state', '!=', 'cancel'),
             '|',
             '&',
             ('date_from', '<=', payslip.date_to),
             ('date_to', '>=', payslip.date_to),
             '|',
             '&',
             ('date_from', '<=', payslip.date_from),
             ('date_to', '>=', payslip.date_from),
             '&',
             ('date_from', '>=', payslip.date_from),
             ('date_to', '<=', payslip.date_to)
             ]
        )

        if payslip_rec:
            raise UserError(_('Exists ! Already a payslip exists for %s and date') % (payslip.employee_id.name))


    def action_payslip_done(self):

        # for payslip in self:
        #     payslip.check_is_record_exist(payslip)
        #     unpaid_id = payslip.line_ids.filtered(lambda line: line.code in ['AV', 'LL', 'EO', 'UV', 'A'])
        #     for unpaid in unpaid_id:
        #         unpaid.rest_cash_hrs_amount = unpaid.rest_cash_hrs_amount + unpaid_id.amount
        #         if unpaid.rest_cash_hrs_amount == 0:
        #             unpaid.payslip_status = True


        for payslip in self:
            payslip.check_is_record_exist(payslip)
            unpaid_id = payslip.getUnpaid_id(payslip.contract_id, payslip.date_from, payslip.date_to)
            for unpaid in unpaid_id:
                financial_clearance = unpaid.financial_clearance
                if financial_clearance == True:
                    if payslip.Days_Number!=0:
                       unpaid.payslip_status = True
                    # hrs_amount = unpaid.rest_cash_hrs_amount
                    # cash_amount = hrs_amount
                    # if cash_amount > payslip.contract_id.wage:
                    #     cash_amount = payslip.contract_id.wage
                    #
                    # unpaid.rest_cash_hrs_amount = unpaid.rest_cash_hrs_amount - cash_amount
                    # if unpaid.rest_cash_hrs_amount == 0:
                    #     unpaid.payslip_status = True

        return super(PayslipUnpaidLeave, self).action_payslip_done()


