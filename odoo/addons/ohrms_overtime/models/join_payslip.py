# -*- coding: utf-8 -*-
from odoo import models, api, fields
from datetime import datetime
from datetime import timedelta


class PayslipDeductJoin(models.Model):
    _inherit = 'hr.payslip'

    ded_join_ids = fields.Many2many('hr.contract')

    @api.model
    def get_inputs(self, contracts, date_from, date_to,employee_id=False,leave_days_number=0,is_resignation=False):
        """
        function used for writing late check-in record in payslip
        input tree.

        """
        res = super(PayslipDeductJoin, self).get_inputs(contracts, date_from, date_to,employee_id,leave_days_number)
        contract = contracts
        join_id = self.env['hr.contract'].search([('employee_id', '=', employee_id),
                                                  ('date_start', '>', date_from),
                                                  ('date_start', '<', date_to),
                                                  ('state', '=', 'open'), ('payslip_paid', '=', False)])
        fmt = '%Y-%m-%d'
        if join_id:
           self.ded_join_ids = join_id 
           date_ded = join_id.date_start
           date_pay = date_from
        #d1=datetime.strftime(date_ded,'%Y-%m-%d')
        #d2=datetime.strftime(date_pay,'%Y-%m-%d')
        #d3 = datetime.datetime.strptime(d1, fmt)
        #d4 = datetime.datetime.strptime(d2, fmt)

           num_days = (date_ded - date_pay).days
           hrs_amount = join_id.over_day * num_days
        if join_id:
            self.ded_join_ids = join_id
            input_data = {
                'name': 'LATEJOIN',
                'code': 'LJD100',
                'amount': hrs_amount,
                'contract_id': contract.id,
            }
            res.append(input_data)
        return res

    def action_payslip_done(self):
           

        for recd in self.ded_join_ids:
               recd.payslip_paid = True
        return super(PayslipDeductJoin, self).action_payslip_done()
