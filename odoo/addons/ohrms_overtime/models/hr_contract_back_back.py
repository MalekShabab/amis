from odoo import models, fields, api


class HrContractOvertime(models.Model):
    _inherit = 'hr.contract'
### Allowances added here in futrue we will migrate it in deferent modeule
    over_hour = fields.Monetary('Hour Wage')
    over_day = fields.Monetary('Day Wage')
    #over_compute = fields.Monetary(compute='_get_wage_employee')
    saudi = fields.Boolean(string="Saudian", default=False)
    accommodation = fields.Boolean(string="Housing", default=False)
    car = fields.Boolean(string="Transportation", default=False)
    Oncall = fields.Boolean(string="On Call", default=False)
    hardship = fields.Boolean(string="Hardship", default=False)

###this fields for deduction days from employees who join to the company not in the the first day of month in future we will migrate it to the different module
    cash_hrs_amount = fields.Monetary('Deduction Join Date')
    payslip_paid = fields.Boolean('Paid in Payslip', readonly=True)
    lday = fields.Monetary('Annual Day Cash')
    lhr = fields.Monetary('Annual Hour Cash')
    gday = fields.Monetary('Leave Day Cash')
    gl = fields.Monetary('Leave Hour Cash')
    uday = fields.Monetary('Unpaid Day Cash')
    ul = fields.Monetary('Unpaid Hour Cash')

    
## The below method for calulating the cost of hour and day for each employee in future we will migrate it to the different module

    @api.onchange('wage')
    def _get_wage_employee(self):
        for sheet in self:
            if sheet.wage:
               sheet.update({
                    'over_day': sheet.wage/30,
                    'over_hour': sheet.wage/240,
                })
            else:
               sheet.update({
                    'over_day': 0,
                    'over_hour': 0,
                })
          
        for abc in self:

             if abc.accommodation == True and abc.car == False:
                abc.update({
                    'lday': (abc.wage*0.25 + abc.house_fixed_allowance + abc.meal_allowance + abc.transportation_fixed_allowance + abc.other_allowance + abc.other_allocations + abc.wage)/30,
                    'lhr': ((abc.wage*0.25 + abc.house_fixed_allowance + abc.meal_allowance +  abc.transportation_fixed_allowance + abc.other_allowance + abc.other_allocations + abc.wage)/30)/8,
                })
             elif abc.accommodation == False and abc.car == True:
                  abc.update({
                    'lday': (abc.wage*0.1 + abc.house_fixed_allowance + abc.meal_allowance + abc.transportation_fixed_allowance + abc.other_allowance + abc.other_allocations + abc.wage)/30,
                    'lhr': ((abc.wage*0.1 + abc.house_fixed_allowance + abc.meal_allowance + abc.transportation_fixed_allowance + abc.other_allowance  + abc.other_allocations+ abc.wage)/30)/8,
                    })
             elif abc.accommodation == True and abc.car == True:
                  abc.update({
                    'lday': (abc.wage*0.10 + abc.wage*0.25 + abc.meal_allowance + abc.house_fixed_allowance + abc.transportation_fixed_allowance + abc.other_allocations + abc.other_allowance + abc.wage)/30,
                    'lhr': ((abc.wage*0.10 + abc.wage*0.25 + abc.meal_allowance + abc.house_fixed_allowance + abc.transportation_fixed_allowance + abc.other_allocations + abc.other_allowance + abc.wage)/30)/8,
                    })
             else:
                  abc.update({
                    'lday': (abc.house_fixed_allowance + abc.meal_allowance + abc.transportation_fixed_allowance + abc.other_allowance + abc.other_allocations + abc.wage)/30,
                    'lhr': ((abc.house_fixed_allowance + abc.meal_allowance + abc.transportation_fixed_allowance + abc.other_allowance + abc.other_allocations + abc.wage)/30)/8,
                    })
        for gbc in self:
              if gbc.accommodation == True:
                 gbc.update({
                    'gday': (gbc.wage*0.25 +  gbc.wage)/30,
                    'gl': ((gbc.wage*0.25 +  gbc.wage)/30)/8,
                })
              
              else:
                   gbc.update({
                    'gday': (gbc.house_fixed_allowance +  gbc.wage)/30,
                    'gl': ((gbc.house_fixed_allowance +  gbc.wage)/30)/8,
                })

        for xbc in self:

            if xbc.accommodation == True and xbc.car == False and xbc.project_allowance == 'a':
                xbc.update({
                    'uday': (xbc.wage * 0.25 + xbc.house_fixed_allowance + xbc.meal_allowance + xbc.transportation_fixed_allowance + xbc.travel_allowance + xbc.other_allowance + xbc.project_allowance_other + 1700  + xbc.other_allocations + xbc.wage) / 30,
                    'ul': ((xbc.wage * 0.25 + xbc.house_fixed_allowance + xbc.meal_allowance + xbc.transportation_fixed_allowance + xbc.travel_allowance + xbc.other_allowance + xbc.project_allowance_other + 1700  + xbc.other_allocations + xbc.wage) / 30) / 8,
                })
            elif xbc.accommodation == False and xbc.car == True and xbc.project_allowance == 'a':
                xbc.update({
                    'uday': (xbc.wage * 0.1 + xbc.house_fixed_allowance + xbc.meal_allowance + xbc.transportation_fixed_allowance + xbc.travel_allowance + xbc.other_allowance + xbc.project_allowance_other + 1700  + xbc.other_allocations + xbc.wage) / 30,
                    'ul': ((xbc.wage * 0.1 + xbc.house_fixed_allowance + xbc.meal_allowance + xbc.transportation_fixed_allowance + xbc.travel_allowance + xbc.other_allowance + xbc.project_allowance_other + 1700  + xbc.other_allocations + xbc.wage) / 30) / 8,
                })
            elif xbc.accommodation == True and xbc.car == True and xbc.project_allowance == 'a':
                xbc.update({
                    'uday': (xbc.wage * 0.25 + xbc.wage * 0.1 + xbc.house_fixed_allowance + xbc.meal_allowance + xbc.transportation_fixed_allowance + xbc.travel_allowance + xbc.project_allowance_other + 1700  + xbc.other_allocations + xbc.other_allowance + xbc.wage) / 30,
                    'ul': ((xbc.wage * 0.25 + xbc.wage * 0.1 + xbc.house_fixed_allowance + xbc.meal_allowance + xbc.transportation_fixed_allowance + xbc.travel_allowance + xbc.project_allowance_other + 1700  + xbc.other_allocations + xbc.other_allowance + xbc.wage) / 30) / 8,
                })
            elif xbc.accommodation == True and xbc.car == False and xbc.project_allowance == 'b':
                xbc.update({
                    'uday': (xbc.wage * 0.25 + xbc.house_fixed_allowance + xbc.meal_allowance + xbc.transportation_fixed_allowance + xbc.travel_allowance + xbc.other_allowance + xbc.project_allowance_other + 1800  + xbc.other_allocations + xbc.wage) / 30,
                    'ul': ((xbc.wage * 0.25 + xbc.house_fixed_allowance + xbc.meal_allowance + xbc.transportation_fixed_allowance + xbc.travel_allowance + xbc.other_allowance + xbc.project_allowance_other + 1800  + xbc.other_allocations + xbc.wage) / 30) / 8,
                })
            elif xbc.accommodation == False and xbc.car == True and xbc.project_allowance == 'b':
                xbc.update({
                    'uday': (xbc.wage * 0.1 + xbc.house_fixed_allowance + xbc.meal_allowance + xbc.transportation_fixed_allowance + xbc.travel_allowance + xbc.other_allowance + xbc.project_allowance_other + 1800 + xbc.other_allocations + xbc.wage) / 30,
                    'ul': ((xbc.wage * 0.1 + xbc.house_fixed_allowance + xbc.meal_allowance + xbc.transportation_fixed_allowance + xbc.travel_allowance + xbc.other_allowance + xbc.project_allowance_other + 1800 + xbc.other_allocations + xbc.wage) / 30) / 8,
                })
            elif xbc.accommodation == True and xbc.car == True and xbc.project_allowance == 'b':
                xbc.update({
                    'uday': (xbc.wage * 0.25 + xbc.wage * 0.1 + xbc.house_fixed_allowance + xbc.meal_allowance + xbc.transportation_fixed_allowance + xbc.travel_allowance + xbc.project_allowance_other + 1800 + xbc.other_allocations + xbc.other_allowance + xbc.wage) / 30,
                    'ul': ((xbc.wage * 0.25 + xbc.wage * 0.1 + xbc.house_fixed_allowance + xbc.meal_allowance + xbc.transportation_fixed_allowance + xbc.travel_allowance + xbc.project_allowance_other + 1800 + xbc.other_allocations + xbc.other_allowance + xbc.wage) / 30) / 8,
                })
            elif xbc.accommodation == True and xbc.car == False and xbc.project_allowance == 'c':
                xbc.update({
                    'uday': (xbc.wage * 0.25 + xbc.house_fixed_allowance + xbc.meal_allowance + xbc.transportation_fixed_allowance + xbc.travel_allowance + xbc.other_allowance + xbc.project_allowance_other + 1900  + xbc.other_allocations + xbc.wage) / 30,
                    'ul': ((xbc.wage * 0.25 + xbc.house_fixed_allowance + xbc.meal_allowance + xbc.transportation_fixed_allowance + xbc.travel_allowance + xbc.other_allowance + xbc.project_allowance_other + 1900  + xbc.other_allocations + xbc.wage) / 30) / 8,
                })
            elif xbc.accommodation == False and xbc.car == True and xbc.project_allowance == 'c':
                xbc.update({
                    'uday': (xbc.wage * 0.1 + xbc.house_fixed_allowance + xbc.meal_allowance + xbc.transportation_fixed_allowance + xbc.travel_allowance + xbc.other_allowance + xbc.project_allowance_other + 1900  + xbc.other_allocations + xbc.wage) / 30,
                    'ul': ((xbc.wage * 0.1 + xbc.house_fixed_allowance + xbc.meal_allowance + xbc.transportation_fixed_allowance + xbc.travel_allowance + xbc.other_allowance + xbc.project_allowance_other + 1900  + xbc.other_allocations + xbc.wage) / 30) / 8,
                })

            elif xbc.accommodation == True and xbc.car == True and xbc.project_allowance == 'c':
                xbc.update({
                    'uday': (xbc.wage * 0.25 + xbc.wage * 0.1 + xbc.house_fixed_allowance + xbc.meal_allowance + xbc.transportation_fixed_allowance + xbc.travel_allowance + xbc.project_allowance_other + 1900  + xbc.other_allocations + xbc.other_allowance + xbc.wage) / 30,
                    'ul': ((xbc.wage * 0.25 + xbc.wage * 0.1 + xbc.house_fixed_allowance + xbc.meal_allowance + xbc.transportation_fixed_allowance + xbc.travel_allowance + xbc.project_allowance_other + 1900  + xbc.other_allocations + xbc.other_allowance + xbc.wage) / 30) / 8,
                })

            else:
                xbc.update({
                    'uday': (xbc.house_fixed_allowance + xbc.meal_allowance + xbc.transportation_fixed_allowance + xbc.travel_allowance + xbc.other_allowance + xbc.project_allowance_other + 1700  + xbc.other_allocations + xbc.wage) / 30,
                    'ul': ((xbc.house_fixed_allowance + xbc.meal_allowance + xbc.transportation_fixed_allowance + xbc.travel_allowance + xbc.other_allowance + xbc.project_allowance_other + 1700  + xbc.other_allocations + xbc.wage) / 30) / 8,
                })



class Followers(models.Model):
      _inherit = 'mail.followers'

      @api.model
      def create(self, vals):
        if 'res_model' in vals and 'res_id' in vals and 'partner_id' in vals:
            dups = self.env['mail.followers'].search([('res_model', '=',vals.get('res_model')),
                                           ('res_id', '=', vals.get('res_id')),
                                           ('partner_id', '=', vals.get('partner_id'))])
            if len(dups):
                for p in dups:
                    p.unlink()
        return super(Followers, self).create(vals)
