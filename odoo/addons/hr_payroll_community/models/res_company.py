# -*- coding:utf-8 -*-

from odoo import api, fields, models


class HrEmployee(models.Model):
    _inherit = 'res.company'
    _description = 'Company'

    iban = fields.Char(string="Bank IBAN")
    bank_number = fields.Char(string="Bank Number")
    reset_allowances_every_month = fields.Boolean(string="Reset Allowances Every Month")
    last_reset_allowances_date = fields.Date(string="Last Reset Allowances Date", readonly=True)
    next_reset_allowances_date = fields.Date(string="Next Reset Allowances Date", readonly=True)
    day_of_reset_allowances_in_month = fields.Integer(string="Day Of Reset Allowances In Month", default=3)