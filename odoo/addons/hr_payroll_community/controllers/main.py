from odoo import http
from odoo.http import request

class Payslip(http.Controller):

      @http.route('/get_payslip', type='json', auth='user')
      def get_payslips(self):
          payslip_rec = request.env['hr.payslip'].search([])
          payslips = []
          for rec in payslip_rec:
              vals = {
                      'employee': rec.employee_id.name,
                      'id': rec.id,
          #            'amount':rec.amount,
    
                      }
              payslips.append(vals)
          data = {'status': 200, 'response': payslips, 'message': 'Success'}
          return data
