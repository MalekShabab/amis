# -*- coding: utf-8 -*-

from odoo import http
from odoo.http import request


class Reminders(http.Controller):

    @http.route('/hr_reminder/all_reminder', type='json', auth="public")
    def all_reminder(self):
        reminder = []
        MyUid =request.env.uid

        reminderss  =request.env['hr.reminder'].search([('create_uid', '=', MyUid),('active', '=', True)])
        for i in reminderss:
            if i.reminder_active:
                reminder.append(i.name)
        return reminder

    @http.route('/hr_reminder/reminder_active', type='json', auth="public")
    def reminder_active(self, **kwargs):
        reminder_value = kwargs.get('reminder_name')
        value = []

        for i in request.env['hr.reminder'].search([('name', '=', reminder_value),('active', '=', True)]):
            value.append(i.model_name.model)
            value.append(i.model_field.name)
            value.append(i.search_by)
            value.append(i.date_set)
            value.append(i.date_from)
            value.append(i.date_to)
        # peeps=request.env[value[0]].search([('birthday', '=',value[3])])
        # for i in peeps:
        #     print i.work_email
        return value
