# -*- coding: utf-8 -*-
import datetime
from datetime import datetime, timedelta
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

date_format = "%Y-%m-%d"
# RESIGNATION_TYPE = [('resigned', 'Normal Resignation'),
#                     ('fired', 'Fired by the company')]


RESIGNATION_TYPE = [('normalResLess10', 'Normal resignation for less than 10 years'),
                    ('normalResMore10', 'Normal resignation for more than 10 years'),
                    ('endContractNo80', 'End of contract based on Termination of the contract based on Article No 80'),
                    ('endContractNo81', 'End of contract based on Termination of the contract based on Article No 81'),
                    ('terminationContract', 'Termination of contract')
                    ]

class HrResignation(models.Model):
    _name = 'hr.resignation'
    _inherit = 'mail.thread'
    _rec_name = 'employee_id'

    name = fields.Char(string='Order Reference', required=True, copy=False, readonly=True, index=True,
                       default=lambda self: _('New'))
    employee_id = fields.Many2one('hr.employee', string="Employee", required=True, default=lambda self: self.env.user.employee_id.id,
                                  help='Name of the employee for whom the request is creating')
    department_id = fields.Many2one('hr.department', string="Department", related='employee_id.department_id',
                                    help='Department of the employee')
    resign_confirm_date = fields.Date(string="Confirmed Date", readonly=True,
                                      help='Date on which the request is confirmed by the employee.',
                                      track_visibility="always")
    approved_revealing_date = fields.Date(string="Approved Last Day Of Employee",
                                          help='Date on which the request is confirmed by the manager.',
                                          track_visibility="always")
    joined_date = fields.Date(string="Join Date", required=False, readonly=True, related="employee_id.joining_date",
                              help='Joining date of the employee.i.e Start date of the first contract')

    expected_revealing_date = fields.Date(string="Last Day of Employee", required=True,
                                          help='Employee requested date on which he is revealing from the company.')
    reason = fields.Text(string="Reason", required=True, help='Specify reason for leaving the company')
    state = fields.Selection(
        [('draft', 'Draft'), ('confirm', 'Confirm'), ('approved', 'Approved'), ('cancel', 'Rejected')],
        string='Status', default='draft', track_visibility="always")
    resignation_type = fields.Selection(selection=RESIGNATION_TYPE,  help="Select the type of resignation: normal "
                                                                                        "resignation or fired by the company")
    read_only = fields.Boolean(string="check field")
    contract_id = fields.Many2one('hr.contract', string='Contract', readonly=True, help="Employee Contract")
    gratuity_id = fields.Many2one('hr.gratuity', string='Gratuity', readonly=True, help="Employee Gratuity")
    gratuity_name = fields.Char(string="Gratuity", readonly=True, related="gratuity_id.name")
    notice_period = fields.Integer(string="Notice Period", readonly=True, related="contract_id.notice_days")
    contract_name = fields.Char(string="Contract", readonly=True, related="contract_id.name")
    last_renewal_date = fields.Date(string='Last Renewal Date')
    period_of_days = fields.Integer(string="Period Of Days", readonly=True)


    @api.onchange('last_renewal_date', 'approved_revealing_date')
    @api.depends('last_renewal_date', 'approved_revealing_date')
    def _get_last_resignation(self):
        if self.last_renewal_date and self.approved_revealing_date:
            self.period_of_days = (self.approved_revealing_date - self.last_renewal_date).days



    @api.onchange('employee_id')
    @api.depends('employee_id')
    def _compute_read_only(self):
        """ Use this function to check weather the user has the permission to change the employee"""
        res_user = self.env['res.users'].search([('id', '=', self._uid)])
        print(res_user.has_group('hr.group_hr_user'))
        if res_user.has_group('hr.group_hr_user'):
            self.read_only = True
        else:
            self.read_only = False

    @api.model
    def create(self, vals):
        if vals['employee_id']:
           no_of_contract = self.env['hr.contract'].search([('employee_id', '=', vals['employee_id'])])
           for contracts in no_of_contract:
                if contracts.state == 'open':
                     vals['contract_id'] = contracts.id
        # assigning the sequence for the record
        if vals.get('name', _('New')) == _('New'):
            vals['name'] = self.env['ir.sequence'].next_by_code('hr.resignation') or _('New')

        # employee = self.env['hr.employee'].search([('id', '=', vals['employee_id'])])
        # vals['name'] = employee.name +'-'+ vals['name']
        res = super(HrResignation, self).create(vals)
        return res

    def remove_relations(self):
        if self.gratuity_id:
           self.env['hr.gratuity'].search([('id', '=', self.gratuity_id.id)]).unlink()

    def unlink(self):
        self.remove_relations()
        super(HrResignation, self).unlink()

    @api.constrains('employee_id')
    def check_employee(self):
        # Checking whether the user is creating leave request of his/her own
        for rec in self:
            if not self.env.user.has_group('hr.group_hr_user'):
                if rec.employee_id.user_id.id and rec.employee_id.user_id.id != self.env.uid:
                    raise ValidationError(_('You cannot create request for other employees'))

    @api.onchange('employee_id')
    @api.depends('employee_id')
    def check_request_existence(self):

        for rec in self:
            if rec.employee_id:
                resignation_request = self.env['hr.resignation'].search([('employee_id', '=', rec.employee_id.id),
                                                                         ('state', 'in', ['confirm', 'draft'])])
                if resignation_request:
                    raise ValidationError(_('There is a resignation request in confirmed or'
                                            ' draft state for this employee'))
                if rec.employee_id:
                    rec.remove_relations()
                    no_of_contract = self.env['hr.contract'].search([('employee_id', '=', self.employee_id.id)])
                    for contracts in no_of_contract:
                        if contracts.state == 'open':
                            rec.contract_id = contracts.id

    @api.constrains('joined_date')
    def _check_dates(self):
        # validating the entered dates
        for rec in self:
            resignation_request = self.env['hr.resignation'].search([('employee_id', '=', rec.employee_id.id),
                                                                     ('state', 'in', ['confirm', 'draft'])])
            if resignation_request:
                raise ValidationError(_('There is a resignation request in confirmed or'
                                        ' draft state for this employee'))

    def confirm_resignation(self):
        if self.joined_date:
            if self.joined_date >= self.expected_revealing_date:
                raise ValidationError(_('Last date of the Employee must be anterior to Joining date'))
            for rec in self:
                rec.state = 'confirm'
                rec.resign_confirm_date = str(datetime.now())
        else:
            raise ValidationError(_('Please set joining date for employee'))

    def cancel_resignation(self):
        for rec in self:
            rec.state = 'cancel'

    def reject_resignation(self):
        for rec in self:
            rec.state = 'cancel'

    def reset_to_draft(self):
        for rec in self:
            rec.state = 'draft'
            rec.employee_id.active = True
            rec.employee_id.resigned = False
            rec.employee_id.fired = False

    def approve_resignation(self):
        for rec in self:
            if rec.expected_revealing_date and rec.resign_confirm_date:
                no_of_contract = self.env['hr.contract'].search([('employee_id', '=', self.employee_id.id)])
                for contracts in no_of_contract:
                    if contracts.state == 'open':
                        rec.contract_id = contracts.id
                        rec.state = 'approved'
                        #rec.approved_revealing_date = rec.resign_confirm_date + timedelta(days=contracts.notice_days)
                    else:
                        rec.approved_revealing_date = rec.expected_revealing_date
                # Changing state of the employee if resigning today
                if rec.expected_revealing_date <= fields.Date.today() and rec.employee_id.active:
                    rec.employee_id.active = False
                    # Changing fields in the employee table with respect to resignation
                    rec.employee_id.resign_date = rec.expected_revealing_date
                    if rec.resignation_type == 'resigned':
                        rec.employee_id.resigned = True
                    else:
                        rec.employee_id.fired = True
                    # Removing and deactivating user
                    if rec.employee_id.user_id:
                        rec.employee_id.user_id.active = False
                        rec.employee_id.user_id = None
                res = {
                            'employee_id': rec.employee_id.id,
                            'name': "Gratuity Settlement For " + rec.employee_id.name,
                }
                gratuity = self.env['hr.gratuity'].create(res)
                gratuity._onchange_employee_id()
                self.gratuity_id = gratuity.id
            else:
                raise ValidationError(_('Please enter valid dates.'))

    def update_employee_status(self):
        resignation = self.env['hr.resignation'].search([('state', '=', 'approved')])
        for rec in resignation:
            if rec.expected_revealing_date <= fields.Date.today() and rec.employee_id.active:
                rec.employee_id.active = False
                # Changing fields in the employee table with respect to resignation
                rec.employee_id.resign_date = rec.expected_revealing_date
                if rec.resignation_type == 'resigned':
                    rec.employee_id.resigned = True
                else:
                    rec.employee_id.fired = True
                # Removing and deactivating user
                if rec.employee_id.user_id:
                    rec.employee_id.user_id.active = False
                    rec.employee_id.user_id = None


class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    resign_date = fields.Date('Resign Date', readonly=True, help="Date of the resignation")
    resigned = fields.Boolean(string="Resigned", default=False, store=True,
                              help="If checked then employee has resigned")
    fired = fields.Boolean(string="Fired", default=False, store=True, help="If checked then employee has fired")
