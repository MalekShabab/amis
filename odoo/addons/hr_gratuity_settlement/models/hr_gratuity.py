from odoo import fields, models, api, _
from odoo.exceptions import Warning, UserError
from odoo import models, api, fields
from datetime import date,timedelta
from odoo.tools.float_utils import float_round

class EmployeeGratuity(models.Model):
    _name = 'hr.gratuity'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = "Employee Gratuity"

    state = fields.Selection([
        ('draft', 'Draft'),
        ('submit', 'Submitted'),
        ('approve', 'Approved'),
        ('cancel', 'Cancelled')],
        default='draft', track_visibility='onchange')
    name = fields.Char(string='Reference', required=True, copy=False,
                       readonly=True,
                       default=lambda self: _('New'))
    employee_id = fields.Many2one('hr.employee', string='Employee',readonly=True,
                                  required=True, help="Employee")
    employee_contract_type = fields.Selection([
        ('limited', 'Limited'),
        ('unlimited', 'Unlimited')], string='Contract Type', readonly=True)
    employee_joining_date = fields.Date(string='Joining Date', readonly=True,
                                        store=True, help="Employee joining date")
    wage_type = fields.Selection([('monthly', 'Monthly Fixed Wage'), ('hourly', 'Hourly Wage')],
                                 help="Select the wage type monthly or hourly")
    total_working_years = fields.Float(string='Total Years Worked', readonly=True, store=True,
                                       help="Total working years")
    employee_leaves_years = fields.Float(string='Leaves Taken', readonly=True, store=True)
    employee_gratuity_years = fields.Float(string='Gratuity Calculation',
                                           readonly=True, store=True, help="Employee gratuity years")
    employee_basic_salary = fields.Float(string='Basic Salary',
                                         readonly=True,
                                         help="Employee's basic salary.")
    employee_gratuity_duration = fields.Many2one('gratuity.configuration',
                                                 readonly=True,
                                                 string='Configuration Line')
    employee_gratuity_configuration = fields.Many2one('hr.gratuity.accounting.configuration',
                                                      readonly=True,
                                                      string='Gratuity Configuration')
    employee_gratuity_amount = fields.Float(string='Gratuity Payment', readonly=True, store=True)
    total_employee_gratuity_amount = fields.Float(string='Total Gratuity Payment', readonly=True, store=True,
                                            help="Total Gratuty After Adding The Leaves Cash And Allowances")
    company_id = fields.Many2one('res.company', 'Company', required=True,readonly=True,
                                 default=lambda self: self.env.company, help="Company")
    currency_id = fields.Many2one(related="company_id.currency_id",
                                  string="Currency", readonly=True, help="Currency")
    currency_name = fields.Char(string="Currency", readonly=True, related="currency_id.name")

    annual_vacation_remaining = fields.Float(string='Annual Vacation', readonly=True, store=True,
                                            help="Number Of Annual Vacation")
    annual_vacation_remaining_cash = fields.Float(string='Annual Vacation', readonly=True, store=True)
    total_loan_lines_remaining = fields.Float(string='Remaining loan installments', readonly=True)
    absence_num = fields.Float(string='Absence', readonly=True, store=True,
                                       help="Number Of Absence")
    unpaid_vacation_num = fields.Float(string='Unpaid Vacation', readonly=True, store=True,
                                            help="Number Of Unpaid Vacation")
    contract_id = fields.Many2one('hr.contract', string="Contract",
                                  related="employee_id.contract_id",
                                  )
    payslip_id = fields.Many2one('hr.payslip', string='Last Payslip', readonly=True, help="Payslip")
    payslip_net_salary = fields.Float(string="Net Salary For Last Payslip", readonly=True, related="payslip_id.Net_salary")
    payslip_name = fields.Char(string="Last Payslip", readonly=True, related="payslip_id.name")
    is_resignation = fields.Boolean(string='Resignation Gratuity', default=False)


    def unlink(self):
        self.remove_relations()
        super(EmployeeGratuity, self).unlink()

    def remove_relations(self):
        if self.payslip_id:
           self.env['hr.payslip'].search([('id', '=', self.payslip_id.id)]).unlink()

    def _gratuity_compute_loan_amount(self):
        loan_ids = self.env['hr.loan.line'].search([('employee_id', '=', self.employee_id.id),
                                                         ('state', '=', 'approve'),
                                                         ('loan_id', '!=', []),
                                                         ('paid', '=', False)])
        if loan_ids:
            loan_ids_amount = loan_ids.mapped('amount')
            self.total_loan_lines_remaining = sum(loan_ids_amount)
            for loan_line in loan_ids:
                if loan_line:
                    loan_line.paid = True
                    loan_line.loan_id._compute_loan_amount()


    @api.model
    def create(self, vals):
        # if not vals['is_resignation']:
        #     raise Warning(_('Sorry but the gratuity could not be created manually...!\n'
        #                     'You should use the resignation module to create a Gratuity'))

        """ assigning the sequence for the record """
        if not vals['name']:
         vals['name'] = self.env['ir.sequence'].next_by_code('hr.gratuity')
        return super(EmployeeGratuity, self).create(vals)

    def _get_remaining_leaves(self):
        self._cr.execute("""
             SELECT
                 sum(h.number_of_days) AS days,
                 h.employee_id
             FROM
                 (
                     SELECT holiday_status_id, number_of_days,
                         state, employee_id
                     FROM hr_leave_allocation
                     UNION ALL
                     SELECT holiday_status_id, (number_of_days * -1) as number_of_days,
                         state, employee_id
                     FROM hr_leave
                 ) h
                 join hr_leave_type s ON (s.id=h.holiday_status_id)
             WHERE
                 s.active = true AND h.state='validate' AND
                 (s.allocation_type='fixed' OR s.allocation_type='fixed_allocation') AND
                 h.employee_id =""" + str(self.employee_id.id) + """GROUP BY h.employee_id""")

        remaining_leave = self._cr.dictfetchall()
        return remaining_leave[0]['days']

    def _set_employee_remaining_leaves(self):
        remaining = self._get_remaining_leaves()
        return float_round(remaining, precision_digits=2)

    def _set_payslip(self, from_date, to_date, employee_id):
            slip_data = self.env['hr.payslip'].onchange_employee_id(from_date, to_date, employee_id, contract_id=False, is_resignation=True)
            res = {
                'employee_id': employee_id,
                'name': "Resignation Payslip For " + self.employee_id.name,
                'struct_id': slip_data['value'].get('struct_id'),
                'contract_id': slip_data['value'].get('contract_id'),
                'input_line_ids': [(0, 0, x) for x in slip_data['value'].get('input_line_ids')],
                'worked_days_line_ids': [(0, 0, x) for x in slip_data['value'].get('worked_days_line_ids')],
                'date_from': from_date,
                'date_to': to_date,
                'company_id': self.company_id.id,
                'is_resignation': True
            }
            payslip = self.env['hr.payslip'].create(res)
            payslip.compute_sheet(is_resignation=True)
            self.payslip_id = payslip.id
            self._gratuity_compute_loan_amount()

    def _set_employee_total_work(self):
        if self.employee_id.id:
            contract_ids = self.env['hr.contract'].search([('employee_id', '=', self.employee_id.id)])
            contract_sorted = contract_ids.sorted(lambda line: line.date_start)
            if not contract_sorted:
                raise Warning(_('No contracts found for the selected employee...!\n'
                                'Employee must have at least one contract to compute gratuity settelement.'))

            hr_contract_id = self.env['hr.contract'].search(
                [('employee_id', '=', self.employee_id.id), ('state', '=', 'open')])
            if len(hr_contract_id) > 1 or not hr_contract_id:
                raise Warning(_('Selected employee have multiple or no running contracts!'))

            leaves = self._get_unpaid_vacation(contract_sorted[-1])
            leaves_days = leaves.mapped('number_of_days')
            leaves_days_amount = sum(leaves_days)
            self.unpaid_vacation_num = leaves_days_amount

            absence = self._get_Absence(contract_sorted[-1])
            absence_days = absence.mapped('number_of_days')
            absence_days_amount = sum(absence_days)
            self.absence_num = absence_days_amount

            self.annual_vacation_remaining = self._set_employee_remaining_leaves()
            self.annual_vacation_remaining_cash = self.annual_vacation_remaining * contract_sorted[-1].lday

            self.employee_joining_date = joining_date = contract_sorted[-1].date_start
            employee_resignation = self.env['hr.resignation'].search(
                [('employee_id', '=', self.employee_id.id), ('state', '=', 'approved')])
            employee_resignation = employee_resignation.sorted(lambda line: line.approved_revealing_date)
            if not employee_resignation or employee_resignation == False:
                raise Warning(_('Employee must resignation request ...'))

            self._set_payslip(employee_resignation[-1].approved_revealing_date.strftime('%Y-%m-01'), employee_resignation[-1].approved_revealing_date, self.employee_id.id)


            self.wage_type = hr_contract_id.wage_type
            if self.wage_type == 'hourly':
                self.employee_basic_salary = hr_contract_id.hourly_wage
            else:
                self.employee_basic_salary = hr_contract_id.wage
            if hr_contract_id.date_end:
                self.employee_contract_type = 'limited'
            else:
                self.employee_contract_type = 'unlimited'
            employee_working_days = (employee_resignation[-1].approved_revealing_date - joining_date).days + 1
            self.total_working_years = employee_working_days / 360
            # if self.total_working_years < 1 and self.employee_id.id:
            #     raise Warning(_('Selected Employee is not eligible for Gratuity Settlement'))
            self.employee_leaves_years = leaves_days_amount / 360
            self.employee_gratuity_years = self.total_working_years - self.employee_leaves_years

    def _get_general_resignation_role(self):
        gratuity_duration_id = False
        current_date = date.today()
        hr_accounting_configuration_id = self.env[
            'hr.gratuity.accounting.configuration'].search(
            [('active', '=', True), ('config_contract_type', '=', self.employee_contract_type),
             '|', ('gratuity_end_date', '>=', current_date), ('gratuity_end_date', '=', False),
             '|', ('gratuity_start_date', '<=', current_date), ('gratuity_start_date', '=', False)])
        if len(hr_accounting_configuration_id) > 1:
            raise UserError(_(
                "There is a date conflict in Gratuity accounting configuration. "
                "Please remove the conflict and try again!"))
        elif not hr_accounting_configuration_id:
            raise UserError(
                _('No gratuity accounting configuration found '
                  'or please set proper start date and end date for gratuity configuration!'))
        ## find configuration ids related to the gratuity accounting configuration
        self.employee_gratuity_configuration = hr_accounting_configuration_id.id
        conf_ids = hr_accounting_configuration_id.gratuity_configuration_table.mapped('id')
        hr_duration_config_ids = self.env['gratuity.configuration'].browse(conf_ids)
        for duration in hr_duration_config_ids:
            if duration.from_year and duration.to_year and duration.from_year <= self.total_working_years <= duration.to_year:
                gratuity_duration_id = duration
                break
            elif duration.from_year and not duration.to_year and duration.from_year <= self.total_working_years:
                gratuity_duration_id = duration
                break
            elif duration.to_year and not duration.from_year and self.total_working_years <= duration.to_year:
                gratuity_duration_id = duration
                break

        if gratuity_duration_id:
            self.employee_gratuity_duration = gratuity_duration_id.id
        else:
            raise Warning(_('No suitable gratuity durations found !'))

    def _get_transportation_allowance(self):
         if self.contract_id.car == True:
            return 0.10 * self.contract_id.wage
         return 0

    def _get_housing_allowance(self):
         if self.contract_id.accommodation == True:
            return 0.25 * self.contract_id.wage
         return 0

    def _get_sub_resignation_role(self):
        sub_resignation_percent = 0
        employee_resignation = self.env['hr.resignation'].search(
            [('employee_id', '=', self.employee_id.id), ('state', '=', 'approved')])

        if not employee_resignation or employee_resignation == False:
            raise Warning(_('Employee must resignation request ...'))

        employee_resignation_type = employee_resignation[0].resignation_type
        if employee_resignation_type == 'normalResLess10':
            if self.total_working_years <= 2:
                sub_resignation_percent = 0
            elif self.total_working_years > 2 and self.total_working_years <= 3:
                sub_resignation_percent = 0.33
            elif self.total_working_years > 5 and self.total_working_years <= 10:
                sub_resignation_percent = 0.6667
            elif self.total_working_years > 10:
                sub_resignation_percent = 1
        elif employee_resignation_type == 'normalResMore10':
            sub_resignation_percent = 1
        else:
            sub_resignation_percent = 0

        return sub_resignation_percent

    def _get_unpaid_vacation(self, contract):

        leaves = self.env['hr.leave'].search(
            [
             ('employee_id', '=', contract.employee_id.id),
             ('contract_id', '=', contract.id),
             ('holiday_status_code', 'in', ['UV']),
             ('state', '=', 'validate'),
             #('payslip_status', '=', False),
             ('date_to', '>=', contract.date_start),
             ]
           )
        return leaves

    def _get_Absence(self, contract):

        leaves = self.env['hr.leave'].search(
            [
             ('employee_id', '=', contract.employee_id.id),
             ('contract_id', '=', contract.id),
             ('holiday_status_code', 'in', ['A']),
             ('state', '=', 'validate'),
             #('payslip_status', '=', False),
             ('date_to', '>=', contract.date_start),
             ]
        )
        return leaves

    def _get_Annual_vacation(self, contract):

        leaves = self.env['hr.leave'].search(
            [
             ('employee_id', '=', contract.employee_id.id),
             ('contract_id', '=', contract.id),
             ('holiday_status_code', 'in', ['AV']),
             ('state', '=', 'validate'),
             #('payslip_status', '=', False),
             ('date_to', '>=', contract.date_start),
             ]
        )
        return leaves



    @api.depends('employee_id')
    @api.onchange('employee_id')
    def _onchange_employee_id(self):
        if self.employee_id.id:
            self._set_employee_total_work()
            sub_resignation_percent = self._get_sub_resignation_role()
            self._get_general_resignation_role()
            transportation_allowance = self._get_transportation_allowance()
            housing_allowance = self._get_housing_allowance()
            self.employee_gratuity_amount = (transportation_allowance + housing_allowance + self.contract_id.wage) * sub_resignation_percent * self.employee_gratuity_duration.percentage * self.employee_gratuity_years
            self.total_employee_gratuity_amount = self.employee_gratuity_amount + self.payslip_id.Net_salary + self.annual_vacation_remaining_cash - self.total_loan_lines_remaining

    # Changing state to submit
    def submit_request(self):
        self.write({'state': 'submit'})

    # Canceling the gratuity request
    def cancel_request(self):
        self.write({'state': 'cancel'})

    # Set the canceled request to draft
    def set_to_draft(self):
        self.write({'state': 'draft'})

    # function for creating the account move with gratuity amount and
    # account credentials
    def approved_request(self):
        # for hr_gratuity_id in self:
        #     debit_vals = {
        #         'name': hr_gratuity_id.employee_id.name,
        #         'account_id': hr_gratuity_id.hr_gratuity_debit_account.id,
        #         'partner_id': hr_gratuity_id.employee_id.address_home_id.id or False,
        #         'journal_id': hr_gratuity_id.hr_gratuity_journal.id,
        #         'date': date.today(),
        #         'debit': hr_gratuity_id.employee_gratuity_amount > 0.0 and hr_gratuity_id.employee_gratuity_amount or 0.0,
        #         'credit': hr_gratuity_id.employee_gratuity_amount < 0.0 and -hr_gratuity_id.employee_gratuity_amount or 0.0,
        #     }
        #     credit_vals = {
        #         'name': hr_gratuity_id.employee_id.name,
        #         'account_id': hr_gratuity_id.hr_gratuity_credit_account.id,
        #         'partner_id': hr_gratuity_id.employee_id.address_home_id.id or False,
        #         'journal_id': hr_gratuity_id.hr_gratuity_journal.id,
        #         'date': date.today(),
        #         'debit': hr_gratuity_id.employee_gratuity_amount < 0.0 and -hr_gratuity_id.employee_gratuity_amount or 0.0,
        #         'credit': hr_gratuity_id.employee_gratuity_amount > 0.0 and hr_gratuity_id.employee_gratuity_amount or 0.0,
        #     }
        #     vals = {
        #         'name': hr_gratuity_id.name + " - " + 'Gratuity for' + ' ' + hr_gratuity_id.employee_id.name,
        #         'narration': hr_gratuity_id.employee_id.name,
        #         'ref': hr_gratuity_id.name,
        #         'partner_id': hr_gratuity_id.employee_id.address_home_id.id or False,
        #         'journal_id': hr_gratuity_id.hr_gratuity_journal.id,
        #         'date': date.today(),
        #         'line_ids': [(0, 0, debit_vals), (0, 0, credit_vals)],
        #     }
        #     move = hr_gratuity_id.env['account.move'].create(vals)
        #     move.post()
        self.write({'state': 'approve'})


class EmployeeContractWage(models.Model):
    _inherit = 'hr.contract'

    # structure_type_id = fields.Many2one('hr.payroll.structure.type', string="Salary Structure Type")
    company_country_id = fields.Many2one('res.country', string="Company country", related='company_id.country_id',
                            readonly=True)
    wage_type = fields.Selection([('monthly', 'Monthly Fixed Wage'), ('hourly', 'Hourly Wage')])
    hourly_wage = fields.Monetary('Hourly Wage', digits=(16, 2), default=0, required=True, tracking=True,
                                  help="Employee's hourly gross wage.")
