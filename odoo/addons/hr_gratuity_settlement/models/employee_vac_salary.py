# -*- coding: utf-8 -*-
from datetime import date
from odoo import fields, models, api, _
from odoo.exceptions import Warning, UserError
from odoo import models, api, fields
#from datetime import date, datetime
import time
import datetime
import calendar
from datetime import date,timedelta


class EmployeeVacSalary(models.Model):
    _name = 'hr.vac.salary'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = "Employee Gratuity"

    state = fields.Selection([
        ('draft', 'Draft'),
        ('submit', 'Submitted'),
        ('approve', 'Approved'),
        ('cancel', 'Cancelled')],
        default='draft', track_visibility='onchange')
    name = fields.Char(string='Reference', required=False, copy=False,
                       readonly=True,
                       default=lambda self: _('New'))
    employee_id = fields.Many2one('hr.employee', string='Employee',
                                  required=True, help="Employee")
    employee_contract_type = fields.Selection([
        ('limited', 'Limited'),
        ('unlimited', 'Unlimited')], string='Contract Type', readonly=True,
        store=True, help="Choose the contract type."
                         "if contract type is limited then during gratuity settlement if you have not specify the end date for contract, gratuity configration of limited type will be taken or"
                         "if contract type is Unlimited then during gratuity settlement if you have specify the end date for contract, gratuity configration of limited type will be taken.")
    employee_joining_date = fields.Date(string='Joining Date', readonly=True,
                                        store=True, help="Employee joining date")
    vac_date_from = fields.Date(string='Date From', store=True, help="Vacation Date From", default=date.today())
    vac_date_to = fields.Date(string='Date To', store=True, help="Vacation Date To", default=date.today())
    total_vac_days = fields.Float(string='Total Vacations Days', readonly=True, store=True,
                                       help="Total Vacation Days", compute="_compute_vacdate_from")
    last_day_month = fields.Date(string='Last Day Of The Month', store=True, help="Last Day Of The Month", compute="_compute_lastdate_from")
    #last_day_month = vac_date_from.max.day
    #last_day_month = datetime.datetime(vac_date_from.year, vac_date_from.month, calendar.mdays[vac_date_from.month])
    total_working_days = fields.Float(string='Total Working Days', readonly=True, store=True,
                                  help="Total Working Days", compute='_compute_working_days')
    perc_working_days = fields.Float(string='Percent Working Days', readonly=True, store=True,
                                      help="Percentage Working Days", compute='_compute_perc_working_days')
    wage_type = fields.Selection([('monthly', 'Monthly Fixed Wage'), ('hourly', 'Hourly Wage')],
                                 help="Select the wage type monthly or hourly")
    total_working_years = fields.Float(string='Total Years Worked', readonly=True, store=True,
                                       help="Total working years")
    employee_probation_years = fields.Float(string='Leaves Taken(Years)', readonly=True, store=True,
                                            help="Employee probation years")
    employee_gratuity_years = fields.Float(string='Gratuity Calculation Years',
                                           readonly=True, store=True, help="Employee gratuity years")
    employee_basic_salary = fields.Float(string='Basic Salary',
                                         readonly=True,
                                         help="Employee's basic salary.")
    employee_annual_salary = fields.Float(string='Leave Salary',
                                         readonly=True,
                                         help="Employee's Leave salary.")
    employee_gratuity_duration = fields.Many2one('gratuity.configuration',
                                                 readonly=True,
                                                 string='Configuration Line')
    employee_gratuity_configuration = fields.Many2one('hr.gratuity.accounting.configuration',
                                                      readonly=True,
                                                      string='Gratuity Configuration')
    employee_gratuity_amount = fields.Float(string='Gratuity Payment', readonly=True, store=True,
                                            help="Gratuity amount for the employee. \it is calculated If the wage type is hourly then gratuity payment is calculated as employee basic salary * Employee Daily Wage Days * gratuity configration rule percentage * gratuity calculation years.orIf the wage type is monthly then gratuity payment is calculated as employee basic salary * (Working Days/Employee Daily Wage Days) * gratuity configration rule percentage * gratuity calculation years.")
    total_employee_gratuity_amount = fields.Float(string='Total Gratuity Payment', readonly=True, store=True,
                                            help="Total Gratuty After Adding The Leaves Cash And Allowances")
    hr_gratuity_credit_account = fields.Many2one('account.account', help="Gratuity credit account")
    hr_gratuity_debit_account = fields.Many2one('account.account', help="Gratuity debit account")
    hr_gratuity_journal = fields.Many2one('account.journal', help="Gratuity journal")
    company_id = fields.Many2one('res.company', 'Company', required=True,
                                 default=lambda self: self.env.company, help="Company")
    currency_id = fields.Many2one(related="company_id.currency_id",
                                  string="Currency", readonly=True, help="Currency")
    unpaid_leave_num = fields.Float(string='Unpaid Leaves', readonly=True, store=True,
                                            help="Number Of Unpaid Leaves")
    annual_leave_num = fields.Float(string='Number Of Annual Leaves', readonly=True, store=True,
                                            help="Number Of Annual Leaves")
    unpleav_ded_cash = fields.Float(string='Unpaid Cash', readonly=True, store=True, help="amount of Unpaid")
    annleav_add_cash = fields.Float(string='Annual Leave Cash', readonly=True, store=True, help="amount of Annual")

    contract_id = fields.Many2one('hr.contract', string="Contract",
                                  related="employee_id.contract_id",
                                  )
    ofa = fields.Float(string='Overtime Fixed Allowance', readonly=True, store=True, help="Overtime Fixed Allowance")
    travel_allowance = fields.Float(string='Travel Allowance', readonly=True, store=True, help="Travel Allowance")
    od = fields.Float(string='Other Deductions', readonly=True, store=True, help="Other Deductions")
    meal_allowance = fields.Float(string='Meal Allowance', readonly=True, store=True, help="Meal Allowance")
    house_fixed_allowance = fields.Float(string='House Fixed Allowance', readonly=True, store=True, help="House Fixed Allowance")
    transportation_fixed_allowance = fields.Float(string='Transportation Fixed Allowance', readonly=True, store=True, help="Transportation Fixed Allowance")
    other_allowance = fields.Float(string='Other Allowance', readonly=True, store=True,
                                                  help="Other Allowance")
    other_allocations = fields.Float(string='Other Allocations', readonly=True, store=True,
                                                  help="Other Allocations")
    project_allowance = fields.Float(string='Project Allowance', readonly=True, store=True,
                                                       help="Project Allowance")
    project_allowance_other = fields.Float(string='Manual Project Allowance', readonly=True, store=True,
                                     help="Manual Project Allowance")
    current_date = fields.Datetime(string="Current Date")
    fixed_allowance = fields.Float(string='Fixed Allowance', readonly=True, store=True, help="Fixed Allowance")
    house_allowance = fields.Float(string='House Allowance', readonly=True, store=True, help="House Allowance")
    transportation_allowance = fields.Float(string='Transportation Allowance', readonly=True, store=True, help="Transportation Allowance")
    oncall_allowance = fields.Float(string='Oncall Allowance', readonly=True, store=True, help="Oncall Allowance")
    hardship_allowance = fields.Float(string='Hardship Allowance', readonly=True, store=True, help="Hardship Allowance")
    overtime_allowance = fields.Float(string='Overtime Allowance', readonly=True, store=True, help="Overtime Allowance")
    total_allowance = fields.Float(string='Total Allowances', readonly=True, store=True, help="Total Allowance")
    total = fields.Float(string='Total', readonly=True, store=True, help="Total")
    loans_ded = fields.Float(string='Loans Deductions', readonly=True, store=True, help="Loans Deductions")
    saudi_ded = fields.Float(string='Insurance Deductions', readonly=True, store=True, help="Insurance Deductions")
    total_ded = fields.Float(string='Total Deductions', readonly=True, store=True, help="Total Deductions")
    net_fin = fields.Float(string='Net Financial Clearance', readonly=True, store=True, help="Net Financial Clearance")



    @api.model
    def create(self, vals):
        """ assigning the sequence for the record """
        vals['name'] = self.env['ir.sequence'].next_by_code('hr.gratuity')
        if vals['employee_id']:
           employee = self.env['hr.employee'].search([('id', '=', vals['employee_id'])])
           vals['name'] = employee.name +' / '+ vals['name']
        return super(EmployeeVacSalary, self).create(vals)

    @api.depends('vac_date_from','vac_date_to')
    def _compute_vacdate_from(self):
        if self.employee_id.id:
            if self.vac_date_to < self.last_day_month and self.vac_date_to >= self.vac_date_from:
                self.total_vac_days = 30
            elif  self.vac_date_to > self.last_day_month:
                self.total_vac_days = (self.vac_date_to - self.vac_date_to.replace(day=1)).days + 31
            elif self.vac_date_to < self.vac_date_from:

                self.total_vac_days = 0

    @api.depends('vac_date_from', 'vac_date_to')
    def _compute_working_days(self):
        self.total_working_days = (self.vac_date_to - self.vac_date_from.replace(day=1)).days

    @api.depends('vac_date_from', 'vac_date_to')
    def _compute_perc_working_days(self):
        self.perc_working_days = self.total_working_days / 30

    @api.depends('vac_date_from', 'vac_date_to')
    def _compute_lastdate_from(self):
        if self.employee_id.id:
            self.last_day_month = datetime.datetime(self.vac_date_from.year, self.vac_date_from.month,
                                               calendar.mdays[self.vac_date_from.month])
           #self.total_vac_days = (self.vac_date_to - self.vac_date_from).days + 1

    @api.onchange('total_vac_days')
    def _onachange_leave_salary(self):
        if self.employee_id.id:
           hr_contract_id = self.env['hr.contract'].search([('employee_id', '=', self.employee_id.id), ('state', '=', 'open')])

           self.employee_annual_salary = round(hr_contract_id.lday * self.total_vac_days, 0)
           self.ofa = hr_contract_id.ofa * self.perc_working_days
           self.od = hr_contract_id.od
           self.travel_allowance = hr_contract_id.travel_allowance * self.perc_working_days
           self.other_allowance = hr_contract_id.other_allowance * self.perc_working_days
           self.other_allocations = hr_contract_id.other_allocations * self.perc_working_days
          # self.project_allowance =  hr_contract_id.project_allowance
           #self.project_allowance = '1700'
           self.project_allowance_other = hr_contract_id.project_allowance_other
           if hr_contract_id.project_allowance == 'a':
              self.project_allowance = 1700 * self.perc_working_days
           elif hr_contract_id.project_allowance == 'b':
              self.project_allowance = 1800 * self.perc_working_days
           elif hr_contract_id.project_allowance == 'c':
              self.project_allowance = 1900 * self.perc_working_days
           else:
               self.project_allowance = 0
           if self.contract_id.car == True:
               car_allowance = self.contract_id.wage * 0.10 * self.perc_working_days
           else:
               car_allowance = 0

           self.transportation_allowance = car_allowance

           if self.contract_id.Oncall == True:
               oncall_allowance = self.contract_id.wage * 0.10 * self.perc_working_days
           else:
               oncall_allowance = 0
           # print(oncall_allowance)

           self.oncall_allowance = oncall_allowance

           if self.contract_id.hardship == True:

               hardship_allowance = self.contract_id.wage * 0.10 * self.perc_working_days
           else:
               hardship_allowance = 0
           #   print(hardship_allowance)

           self.hardship_allowance = hardship_allowance

           if self.contract_id.accommodation == True:
               house_allowance = self.contract_id.wage * 0.25 * self.perc_working_days
           else:
               house_allowance = 0

           self.house_allowance = house_allowance

           overtime_id = self.env['hr.overtime'].search([('employee_id', '=', self.employee_id.id),
                                                         ('state', '=', 'approved'), ('payslip_paid', '=', False), ('date_from', '<', self.vac_date_from) ])
           hrs_amount = overtime_id.mapped('cash_hrs_amount')
           cash_ovt = sum(hrs_amount)
           self.overtime_allowance = cash_ovt

           self.total_allowance = self.travel_allowance + self.other_allowance + self.project_allowance + self.project_allowance_other + self.other_allocations + self.overtime_allowance + self.house_allowance + self.transportation_allowance + self.ofa

           line_id = self.env['hr.loan.line'].search([('employee_id', '=', self.employee_id.id),
                                                      ('state', '=', 'approve'),
                                                      ('loan_id', '!=', []),
                                                      ('date', '>=', self.vac_date_from),
                                                      ('date', '<=', self.vac_date_to),
                                                      ('paid', '=', False)])

           hrs_amount = line_id.mapped('amount')
           cash_amount = sum(hrs_amount)
           self.loans_ded = cash_amount

           if self.contract_id.saudi == True:
               insurance = (self.contract_id.wage + self.house_allowance)  * 0.10
           else:
               insurance = 0

           self.saudi_ded = insurance

           self.total_ded = self.saudi_ded + self.loans_ded + self.od
           self.net_fin = self.employee_annual_salary + self.total_allowance - self.total_ded


    @api.onchange('employee_id')
    def _onchange_employee_id(self):
        """ calculating the gratuity pay based on the contract and gratuity
        configurations """
        if self.employee_id.id:
         #   number_unpaid_leaves = self.env['hr.leave'].search(
           #     [('holiday_status_id', '=', 37)])
           # un_days_amount = number_unpaid_leaves.mapped('number_of_days')
            #un_amount = sum(un_days_amount)
         #   un_amount = 7
           # self.unpaid_leave_num = un_amount

            current_date = date.today()

            probation_ids = self.env['hr.training'].search([('employee_id', '=', self.employee_id.id)])
            contract_ids = self.env['hr.contract'].search([('employee_id', '=', self.employee_id.id)])
            contract_sorted = contract_ids.sorted(lambda line: line.date_start)
            if not contract_sorted:
                raise Warning(_('No contracts found for the selected employee...!\n'
                                'Employee must have at least one contract to compute gratuity settelement.'))
            self.employee_joining_date = joining_date = contract_sorted[0].date_start
            employee_probation_days = 0
            # find total probation days
            for probation in probation_ids:
                start_date = probation.start_date
                end_date = probation.end_date
                employee_probation_days += (end_date - start_date).days
            # get running contract
            hr_contract_id = self.env['hr.contract'].search(
                [('employee_id', '=', self.employee_id.id), ('state', '=', 'open')])
            if len(hr_contract_id) > 1 or not hr_contract_id:
                raise Warning(_('Selected employee have multiple or no running contracts!'))

            self.wage_type = hr_contract_id.wage_type

            if self.wage_type == 'hourly':
                self.employee_basic_salary = hr_contract_id.hourly_wage
            else:
                self.employee_basic_salary = hr_contract_id.wage

            if hr_contract_id.date_end:
                self.employee_contract_type = 'limited'
                employee_working_days = (hr_contract_id.date_end - joining_date).days
                self.total_working_years = employee_working_days / 365
                self.employee_probation_years = employee_probation_days / 365
                #employee_gratuity_years = (employee_working_days - employee_probation_days) / 365
                employee_gratuity_years = (employee_working_days) / 365
                self.employee_gratuity_years = employee_gratuity_years
                number_unpaid_leaves = self.env['hr.leave'].search(
                    [('holiday_status_id', '=', 37),
                     ('emp_number', '=', self.employee_id.id)])
                number_annual_leaves = self.env['hr.leave.allocation'].search(
                    [('emp_number', '=', self.employee_id.id),
                     ('id', '=', 23)])

                num_ann_leav = number_annual_leaves.mapped('number_of_days')
                ann_amount = sum(num_ann_leav)
                cash_an = self.contract_id.over_day
                self.annual_leave_num = ann_amount
                self.annleav_add_cash = cash_an * self.annual_leave_num
                un_days_amount = number_unpaid_leaves.mapped('number_of_days')
                un_amount = sum(un_days_amount)
                self.unpaid_leave_num = un_amount
                cash_am = self.contract_id.gday
                self.unpleav_ded_cash = -cash_am * self.unpaid_leave_num
                total = self.annleav_add_cash + self.unpleav_ded_cash
                self.total = total
                self.current_date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                # self.current_date = datetime.date
                datenow = self.current_date
                todayDate = datetime.date.today()
                resultDate = todayDate.replace(day=1)
                perctime = (todayDate - resultDate).days / 30
               # rtime = round(perctime, 2)
             #   rtime = round(self.perc_working_days, 2)
                if self.contract_id.project_allowance == '1700':
                    fixed_allowances = self.contract_id.ofa + self.contract_id.od + self.contract_id.travel_allowance + self.contract_id.meal_allowance + self.contract_id.house_fixed_allowance + self.contract_id.transportation_fixed_allowance + self.contract_id.other_allowance + 1700
                elif self.contract_id.project_allowance == '1800':
                    fixed_allowances = self.contract_id.ofa + self.contract_id.od + self.contract_id.travel_allowance + self.contract_id.meal_allowance + self.contract_id.house_fixed_allowance + self.contract_id.transportation_fixed_allowance + self.contract_id.other_allowance + 1800
                elif self.contract_id.project_allowance == '1900':
                    fixed_allowances = self.contract_id.ofa + self.contract_id.od + self.contract_id.travel_allowance + self.contract_id.meal_allowance + self.contract_id.house_fixed_allowance + self.contract_id.transportation_fixed_allowance + self.contract_id.other_allowance + 1900
                else:
                    fixed_allowances = self.contract_id.ofa + self.contract_id.od + self.contract_id.travel_allowance + self.contract_id.meal_allowance + self.contract_id.house_fixed_allowance + self.contract_id.transportation_fixed_allowance + self.contract_id.other_allowance  #+ self.contract_id.project_allowance_other
                self.fixed_allowance = fixed_allowances * rtime


            else:
                self.employee_contract_type = 'unlimited'
                #un_amount = 7
                #self.unpaid_leave_num = 7
                employee_working_days = (current_date - joining_date).days
                self.total_working_years = employee_working_days / 365
                self.employee_probation_years = employee_probation_days / 365
                #employee_gratuity_years = (employee_working_days - employee_probation_days) / 365
                employee_gratuity_years = (employee_working_days) / 365
                #self.employee_gratuity_years = round(employee_gratuity_years, 2)
                self.employee_gratuity_years = employee_gratuity_years
              #  un_amount = 7
                number_unpaid_leaves = self.env['hr.leave'].search(
                    [('holiday_status_id', '=', 37),
                     ('emp_number', '=', self.employee_id.id)])
                number_annual_leaves = self.env['hr.leave.allocation'].search(
                    [('emp_number', '=', self.employee_id.id),
                     ('id', '=', 23)])

                num_ann_leav = number_annual_leaves.mapped('number_of_days')
                ann_amount = sum(num_ann_leav)
                cash_an = self.contract_id.over_day
                self.annual_leave_num = ann_amount
                self.annleav_add_cash = cash_an * self.annual_leave_num
                un_days_amount = number_unpaid_leaves.mapped('number_of_days')
                un_amount = sum(un_days_amount)
                self.unpaid_leave_num = un_amount
                cash_am = self.contract_id.gday
                self.unpleav_ded_cash = -cash_am * self.unpaid_leave_num
                total = self.annleav_add_cash + self.unpleav_ded_cash
                self.total = total
                self.current_date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                #self.current_date = datetime.date
                datenow = self.current_date
                todayDate = datetime.date.today()
                resultDate = todayDate.replace(day=1)
                perctime = (todayDate - resultDate).days/30
                rtime = round(perctime, 2)

                if self.contract_id.project_allowance == '1700':
                   fixed_allowances = self.contract_id.ofa + self.contract_id.od + self.contract_id.travel_allowance + self.contract_id.meal_allowance + self.contract_id.house_fixed_allowance + self.contract_id.transportation_fixed_allowance + self.contract_id.other_allowance + 1700
                elif self.contract_id.project_allowance == '1800':
                    fixed_allowances = self.contract_id.ofa + self.contract_id.od + self.contract_id.travel_allowance + self.contract_id.meal_allowance + self.contract_id.house_fixed_allowance + self.contract_id.transportation_fixed_allowance + self.contract_id.other_allowance + 1800
                elif self.contract_id.project_allowance == '1900':
                    fixed_allowances = self.contract_id.ofa + self.contract_id.od + self.contract_id.travel_allowance + self.contract_id.meal_allowance + self.contract_id.house_fixed_allowance + self.contract_id.transportation_fixed_allowance + self.contract_id.other_allowance + 1900
                else:
                   fixed_allowances = self.contract_id.ofa + self.contract_id.od + self.contract_id.travel_allowance + self.contract_id.meal_allowance + self.contract_id.house_fixed_allowance + self.contract_id.transportation_fixed_allowance + self.contract_id.other_allowance + self.contract_id.project_allowance_other
                self.fixed_allowance = fixed_allowances * rtime


            gratuity_duration_id = False
            hr_accounting_configuration_id = self.env[
                'hr.gratuity.accounting.configuration'].search(
                [('active', '=', True), ('config_contract_type', '=', self.employee_contract_type),
                 '|', ('gratuity_end_date', '>=', current_date), ('gratuity_end_date', '=', False),
                 '|', ('gratuity_start_date', '<=', current_date), ('gratuity_start_date', '=', False)])
            if len(hr_accounting_configuration_id) > 1:
                raise UserError(_(
                    "There is a date conflict in Gratuity accounting configuration. "
                    "Please remove the conflict and try again!"))
            elif not hr_accounting_configuration_id:
                raise UserError(
                      _('No gratuity accounting configuration found '
                      'or please set proper start date and end date for gratuity configuration!'))
           ## find configuration ids related to the gratuity accounting configuration
            self.employee_gratuity_configuration = hr_accounting_configuration_id.id
            conf_ids = hr_accounting_configuration_id.gratuity_configuration_table.mapped('id')
            hr_duration_config_ids = self.env['gratuity.configuration'].browse(conf_ids)
            for duration in hr_duration_config_ids:
                if duration.from_year and duration.to_year and duration.from_year <= self.total_working_years <= duration.to_year:
                    gratuity_duration_id = duration
                    break
                elif duration.from_year and not duration.to_year and duration.from_year <= self.total_working_years:
                    gratuity_duration_id = duration
                    break
                elif duration.to_year and not duration.from_year and self.total_working_years <= duration.to_year:
                    gratuity_duration_id = duration
                    break

            if gratuity_duration_id:
                self.employee_gratuity_duration = gratuity_duration_id.id
            else:
                raise Warning(_('No suitable gratuity durations found !'))
            # show warning when the employee's working years is less than
            # one year or no running employee found.
            if self.total_working_years < 1 and self.employee_id.id:
                raise Warning(_('Selected Employee is not eligible for Gratuity Settlement'))
            self.hr_gratuity_journal = hr_accounting_configuration_id.gratuity_journal.id
            self.hr_gratuity_credit_account = hr_accounting_configuration_id.gratuity_credit_account.id
            self.hr_gratuity_debit_account = hr_accounting_configuration_id.gratuity_debit_account.id

            if self.employee_gratuity_duration and self.wage_type == 'hourly':
                if self.employee_gratuity_duration.employee_working_days != 0:
                    if self.employee_id.resource_calendar_id and self.employee_id.resource_calendar_id.hours_per_day:
                        daily_wage = self.employee_basic_salary * self.employee_id.resource_calendar_id.hours_per_day
                    else:
                        daily_wage = self.employee_basic_salary * 8
                    working_days_salary = daily_wage * self.employee_gratuity_duration.employee_working_days
                  # # gratuity_pay_per_year = working_days_salary * self.employee_gratuity_duration.percentage
                  # # employee_gratuity_amount = gratuity_pay_per_year * self.employee_gratuity_years
                    employee_gratuity_amount = self.contract_id.wage * self.employee_gratuity_years * self.employee_gratuity_duration.percentage

                    total_employee_gratuity_amount = employee_gratuity_amount + self.total + self.total_allowance
                    self.employee_gratuity_amount = round(employee_gratuity_amount, 2)
                    self.total_employee_gratuity_amount = round(total_employee_gratuity_amount, 2)
                else:
                    raise Warning(_("Employee working days is not configured in "
                                    "the gratuity configuration..!"))
            elif self.employee_gratuity_duration and self.wage_type == 'monthly':
                if self.employee_gratuity_duration.employee_daily_wage_days != 0:
                    daily_wage = self.employee_basic_salary / self.employee_gratuity_duration.employee_daily_wage_days
                    working_days_salary = daily_wage * self.employee_gratuity_duration.employee_working_days
               #   #  gratuity_pay_per_year = working_days_salary * self.employee_gratuity_duration.percentage
                  # # employee_gratuity_amount = gratuity_pay_per_year * self.employee_gratuity_years
                    employee_gratuity_amount = self.contract_id.wage * self.employee_gratuity_years * self.employee_gratuity_duration.percentage
                    self.employee_gratuity_amount =  employee_gratuity_amount
                    total_employee_gratuity_amount = employee_gratuity_amount + self.total + self.total_allowance

                    self.total_employee_gratuity_amount = round(total_employee_gratuity_amount, 2)
                  #  self.employee_gratuity_amount = round(employee_gratuity_amount, 2)
                else:
                    raise Warning(_("Employee wage days is not configured in "
                                    "the gratuity configuration..!"))




    # Changing state to submit
    def submit_request(self):
        self.write({'state': 'submit'})

    # Canceling the gratuity request
    def cancel_request(self):
        self.write({'state': 'cancel'})

    # Set the canceled request to draft
    def set_to_draft(self):
        self.write({'state': 'draft'})

    # function for creating the account move with gratuity amount and
    # account credentials
    def approved_request(self):
        for hr_gratuity_id in self:
            debit_vals = {
                'name': hr_gratuity_id.employee_id.name,
                'account_id': hr_gratuity_id.hr_gratuity_debit_account.id,
                'partner_id': hr_gratuity_id.employee_id.address_home_id.id or False,
                'journal_id': hr_gratuity_id.hr_gratuity_journal.id,
                'date': date.today(),
                'debit': hr_gratuity_id.employee_gratuity_amount > 0.0 and hr_gratuity_id.employee_gratuity_amount or 0.0,
                'credit': hr_gratuity_id.employee_gratuity_amount < 0.0 and -hr_gratuity_id.employee_gratuity_amount or 0.0,
            }
            credit_vals = {
                'name': hr_gratuity_id.employee_id.name,
                'account_id': hr_gratuity_id.hr_gratuity_credit_account.id,
                'partner_id': hr_gratuity_id.employee_id.address_home_id.id or False,
                'journal_id': hr_gratuity_id.hr_gratuity_journal.id,
                'date': date.today(),
                'debit': hr_gratuity_id.employee_gratuity_amount < 0.0 and -hr_gratuity_id.employee_gratuity_amount or 0.0,
                'credit': hr_gratuity_id.employee_gratuity_amount > 0.0 and hr_gratuity_id.employee_gratuity_amount or 0.0,
            }
            vals = {
                'name': hr_gratuity_id.name + " - " + 'Gratuity for' + ' ' + hr_gratuity_id.employee_id.name,
                'narration': hr_gratuity_id.employee_id.name,
                'ref': hr_gratuity_id.name,
                'partner_id': hr_gratuity_id.employee_id.address_home_id.id or False,
                'journal_id': hr_gratuity_id.hr_gratuity_journal.id,
                'date': date.today(),
                'line_ids': [(0, 0, debit_vals), (0, 0, credit_vals)],
            }
            move = hr_gratuity_id.env['account.move'].create(vals)
            move.post()
        self.write({'state': 'approve'})


class EmployeeContractWage(models.Model):
    _inherit = 'hr.contract'

    # structure_type_id = fields.Many2one('hr.payroll.structure.type', string="Salary Structure Type")
    company_country_id = fields.Many2one('res.country', string="Company country", related='company_id.country_id',
                            readonly=True)
    wage_type = fields.Selection([('monthly', 'Monthly Fixed Wage'), ('hourly', 'Hourly Wage')])
    hourly_wage = fields.Monetary('Hourly Wage', digits=(16, 2), default=0, required=True, tracking=True,
                                  help="Employee's hourly gross wage.")
