13.0.1 (25th Jan 2021)
-------------------------
Initial Release

13.0.2 (Date :29th Jan 2021)
-------------------------------
[ADD] add configuration to filter due/overdue statement records based on configuration.

13.0.3 (Date : 25th March 2021)
---------------------------------
[ADD] add customer and vendor statement xls reports and as well as in portal also print xls reports.

13.0.4 (Date : 9th April 2021)
----------------------------------
[FIX]bug of display both tabs customer and vendor statement if partner is customer and vendor.

13.0.5 (Date : 27th April 2021)
---------------------------------
[FIX] bug fixed of refund invoice/bill balance calculation.

13.0.6 (Date : 26th May 2021)
---------------------------------
[UPDATE] change some amount related fields of statement records.

13.0.7 (Date : 14th June 2021)
-------------------------------------
[FIX] small error fixed.

13.0.8 (Date : 26th July 2021)
--------------------------------------
[ADD] configurations added fro send whatsapp feature.
[ADD] send by whatsapp button added in statement(partner) view.
[ADD] signature field is added in user prefrences.

13.0.9 (Date :13th Aug 2021)
--------------------------------------

[ADD] Summary Statement functionality on cron
[ADD] filter in mail-logs 
[ADD] Multi-action for send summary statement