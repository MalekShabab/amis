# -*- coding: utf-8 -*-

from . import hr_employee
from . import company
from . import loan
from . import fleet
from . import hr_payroll