# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime
from dateutil.relativedelta import relativedelta

class Loan(models.Model):
    _inherit='hr.loan'

    def compute_installment2(self,payment_date):
        date_start = datetime.strptime(str(payment_date), '%Y-%m-%d')
        amount = self.loan_amount / self.installment
        for i in range(1, self.installment + 1):
            self.env['hr.loan.line'].create({
                'date': date_start,
                'amount': amount,
                'employee_id': self.employee_id.id,
                'loan_id': self.id})
            date_start = date_start + relativedelta(months=1)
        self._compute_loan_amount()

    @api.onchange('loan_lines')
    def onchange_loan_lines44(self):
        if self.loan_lines:
            payment_date = self.loan_lines[0].date
        print("payment_date",payment_date)
        for line in self.loan_lines:
            payment_date=payment_date+relativedelta(months=1)
            line.date=payment_date



    @api.depends('loan_lines.paid','loan_lines.amount')
    def _compute_loan_amount(self):
        total_paid = 0.0
        for loan in self:
            for line in loan.loan_lines:
                if line.paid:
                    total_paid += line.amount
            balance_amount = loan.loan_amount - total_paid
            loan.total_amount = loan.loan_amount
            loan.balance_amount = balance_amount
            loan.total_paid_amount = total_paid











