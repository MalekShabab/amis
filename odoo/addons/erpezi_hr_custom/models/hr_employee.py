# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import date
from datetime import datetime
import datetime
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError


class Empolyee(models.Model):
    _inherit = 'hr.employee'

    number = fields.Char(string="Employee(ID)", required=False, )

    # @api.constrains('age')
    # def _check_age(self):
    #     x = 65
    #     y = 18
    #     for rec in self:
    #         if rec.age >= x or rec.age <= y:
    #             raise ValidationError("The Age Is Not Available")
    #
    # @api.onchange('birthday')
    # def _compute_age(self):
    #     for record in self:
    #         if record.birthday and record.birthday <= fields.Date.today():
    #             record.age = relativedelta(
    #                 fields.Date.from_string(fields.Date.today()),
    #                 fields.Date.from_string(record.birthday)).years
    #         else:
    #             record.age = 0

    def clear_monthly_advantages_in_cash(self):
        for contract in self.env['hr.contract'].sudo().search([('state', '=', 'open')]):
            contract.write(
                {
                    'ofa': 0,
                    'travel_allowance': 0,
                    'od': 0,
                    'meal_allowance': 0,
                    'house_fixed_allowance': 0,
                    'medical_allowance': 0,
                    'transportation_fixed_allowance': 0,
                    'other_allowance': 0,
                    'other_allocations': 0,
                }
            )

    def clear_project_allowance(self):
        for contract in self.env['hr.contract'].sudo().search([('state', '=', 'open')]):
            if str(contract.period) == str(datetime.now().date()):
                contract.write(
                    {
                        'project_allowance': 0,
                        'period': False,

                    }
                )


class Contract(models.Model):
    _inherit = 'hr.contract'

    period = fields.Date(string="Period", required=False, )

    def write(self, values):
        res = super(Contract, self).write(values)
        for con in self:
            if 'wage' in values.keys():
                note = "تم تغير المرتب إلي %s" % str(con.wage)
                self.send_email_for_user(con.employee_id.work_email, note)

        return res

    def send_email_for_user(self, email_to, note):
        mail_content = note
        main_content = {
            'subject': note,
            'author_id': self.env.user.partner_id.id,
            'body_html': mail_content,
            'email_to': email_to,
            'state': "outgoing",

        }
        res = self.env['mail.mail'].sudo().create(main_content).send()


class Change(models.Model):
    _inherit = 'hr.contract'

    salary_category = fields.Selection([
        ('iban', 'IBAN'),
        ('card', 'CARD'),
        ('cash', 'CASH'),
    ], 'Salary Category', default='cash', groups="hr.group_hr_user", tracking=True)
    project_allowance = fields.Selection([
        ('a', '1700'),
        ('b', '1800'),
        ('c', '1900'),
        ('d', '0'),
        ('e', '700'),
        ('f', '400'),
        ('g', '300'),
    ], 'Project Allowance', groups="hr.group_hr_user", tracking=True)


class Attendence(models.Model):
    _inherit = 'hr.attendance'

    late_check_in = fields.Float(string="Late Check In", compute='check_late')
    late_check_out = fields.Float(string="Late Check Out", compute='check_late')
    absence = fields.Boolean(string="Absence", required=False, compute='check_late')

    @api.depends('employee_id', 'check_in', 'check_out')
    def check_late(self):
        for rec in self:
            if rec.employee_id.id:
                hour_from, hour_to = False, False
                if rec.check_in:
                    check_in = datetime.datetime.strptime(str(rec.check_in), "%Y-%m-%d %H:%M:%S")
                    check_in_index = check_in.weekday()
                    hour_from, hour_to = self.get_hour_from_to(rec.employee_id, check_in_index)
                    if hour_from:
                        rec.late_check_in = hour_from - (check_in.hour + 2)
                        rec.absence = False
                    else:
                        rec.late_check_in = False
                        rec.absence = True


                else:
                    rec.late_check_in = False
                    rec.absence = True
                if rec.check_out:
                    if hour_to:
                        check_out = datetime.datetime.strptime(str(rec.check_out), "%Y-%m-%d %H:%M:%S")
                        rec.late_check_out = hour_to - (check_out.hour + 2)
                        print("hour_from", hour_to, check_out.hour)

                        rec.absence = False
                    else:
                        check_out = datetime.datetime.strptime(str(rec.check_out), "%Y-%m-%d %H:%M:%S")
                        check_out_index = check_out.weekday()
                        hour_from, hour_to = self.get_hour_from_to(rec.employee_id, check_out_index)
                        if hour_to:
                            rec.late_check_out = hour_to - (check_out.hour + 2)
                            rec.absence = False

                        else:
                            rec.late_check_out = False
                            rec.absence = True
                else:
                    rec.late_check_out = False
                    rec.absence = True
            else:
                rec.late_check_in = False
                rec.late_check_out = False
                rec.absence = False

    def get_hour_from_to(self, employee, index):
        hour_from, hour_to = False, False
        for day in employee.resource_calendar_id.attendance_ids:
            if int(day.dayofweek) == int(index):
                hour_from = day.hour_from
                hour_to = day.hour_to
        return hour_from, hour_to
