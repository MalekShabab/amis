# -*- coding: utf-8 -*-

from odoo import models, fields, api


class HrPayroll(models.Model):
    _inherit = 'hr.payslip'

    number = fields.Char(string="Employee(ID)", required=False, )
