# -*- coding: utf-8 -*-

from odoo import api, fields, models


class Fleet(models.Model):
    _inherit = 'fleet.vehicle'

    date1 = fields.Datetime(string="Reserved_From", required=False, )

    date2 = fields.Datetime(string="Reserved_To", required=False, )
    new_field = fields.Char(string="Number Of Fleets", required=False, )
    state = fields.Selection(string="Fleet Condition",
                             selection=[('available', 'Available'), ('not available', 'Not Available'), ],
                             required=False, )
    fleet_available = fields.Char(string="Fleet Availability", compute='_compute_calc_val', readonly=True, store=True)

    # @api.depends('state', 'fleet_available')
    # def _compute_calc_val(self):
    #     self.state == "available" or "not available"
    #     if self.state == "available":
    #         self.fleet_available = True
    #     else:
    #         self.fleet_available = False

    @api.depends('state')
    def _compute_calc_val(self):
        if self.state == 'available':
            self.fleet_available = True
        else:
            self.fleet_available = False


class Invoice(models.Model):
    _inherit = 'account.move'

    date1 = fields.Datetime(string="Reserved_from", related='model_id.date1', required=False, )

    date2 = fields.Datetime(string="Reserved_to", related='model_id.date2', required=False, )

    model_id = fields.Many2one(comodel_name="fleet.vehicle", string="Truck / Equipment", required=False, )
