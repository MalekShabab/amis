from odoo import api, fields, models
from odoo.exceptions import ValidationError


class Company(models.Model):
    _inherit='res.company'
    project_allowance_end_date = fields.Date(string="Project Allowance End Date", required=False)
