# -*- coding: utf-8 -*-

from odoo import models, fields, api


class invoice_report(models.Model):
    _inherit = 'account.move.line'

    quantity2 = fields.Integer(string="Quantity", )
    number = fields.Integer(string="Number", )
    unit_of_majer = fields.Many2one(comodel_name="sale.unit", string="Unit Of Measure", required=False)

    @api.onchange('number', 'quantity2')
    def _onchange_field(self):
        for rec in self:
            rec.quantity = rec.quantity2 * rec.number


class sale_report(models.Model):
    _inherit = 'sale.order.line'

    quantity2 = fields.Integer(string="Quantity", )
    number = fields.Integer(string="Number", )

    unit_of_majer = fields.Many2one(comodel_name="sale.unit", string="Unit Of Measure", required=False, )

    @api.onchange('number', 'quantity2')
    def _onchange_field(self):
        for rec in self:
            rec.product_uom_qty = rec.quantity2 * rec.number


class sale(models.Model):
    _inherit = 'sale.order'

    ref = fields.Char(string="Reference", required=False, )
