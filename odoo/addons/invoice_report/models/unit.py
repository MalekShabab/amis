# -*- coding: utf-8 -*-

from odoo import models, fields, api


class UnitSales(models.Model):
    _name = 'sale.unit'

    name = fields.Char(string="Unit Name", required=False, )


# class SaleOrder(models.Model):
#     _inherit = 'sale.unit'
#
#     def get_product_unit_name(self,pid):
#         translation = self.env['ir.translation'].search([
#             ('unit','=','sale.unit,name'),('state','=','translated'),
#             ('res_id','=',pid)])
#         if translation :
#             return translation.value
#         else:
#             unit = self.env['sale.unit'].browse(int(pid))
#             translation = self.env['ir.translation'].search([
#                 ('unit','=','sale.unit,name'),('state','=','translated'),
#                 ('res_id','=',sale.unit_tmpl_id.id)])
#             if translation :
#                 return translation.value
#         return ''
