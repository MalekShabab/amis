from odoo import http
from odoo.http import request

class Loan(http.Controller):

      @http.route('/get_loans', type='json', auth='none', csrf=False, cors='*')
      #def get_overtimes(self):
      def loan(self, db, login, password, base_location=None):
          request.session.logout()
          request.session.authenticate(db, login, password)
          loans_rec = request.env['hr.loan'].search([('employee_id.id','=',login)])
          loans = []
          for rec in loans_rec:
              vals = {
                      'employee': rec.employee_id.name,
                      'id': rec.id,
                      'loan_type': rec.loan_type,
                      'amount':rec.loan_amount,
                      'payment date': rec.payment_date,
                      'balance': rec.balance_amount,
                      'duration': rec.installment,
                      'state': rec.state,
    
                      }
              loans.append(vals)
          data = {'status': 200, 'response': loans, 'message': 'Success'}
          return data
      @http.route('/create_loan', type='json', auth='none', csrf=False, cors='*')
      def create_loan(self, db, login, password, loan_type, payment_date, loan_amount, installment):
        request.session.logout()
        request.session.authenticate(db, login, password)
        if request.jsonrequest:
         # print ("rec", rec)
         # if rec['state']:
              vals = {
                      "employee_id":login,
                      "loan_type":loan_type,
                      "payment_date":payment_date,
                      "loan_amount":loan_amount,
                      "installment":installment
              # 'date_from': '2020-09-07 08:08:05',
              # 'date_to': '2020-09-07 08:08:05'
            }
              new_loan = request.env['hr.loan'].sudo().create(vals)
        print("New  Loan Is", new_loan)
        args = {'success': True, 'message': 'Success', 'ID':new_loan.id}
        return args
      @http.route('/delete_loan', type='json', auth='none', csrf=False, cors='*')
      def unlink_loan(self, db, login, password, num):
        request.session.logout()
        request.session.authenticate(db, login, password)
        if request.jsonrequest:
         # print ("rec", rec)
         # if rec['state']:
              vals = num
                     # "id": num

              # 'date_from': '2020-09-07 08:08:05',
              # 'date_to': '2020-09-07 08:08:05'
          #  }
              #del_off = request.env['hr.leave'].sudo().unlink(vals)
        #      request.env['hr.leave'].unlink(vals)



              loan = request.env['hr.loan'].search([('id', '=', num)])
              delete = loan.unlink()
        print("Deleted  Off Is", delete)
        args = {'success': True, 'message': 'Success', 'ID':delete}
        return args

      @http.route('/get_hr_loans', type='json', auth='none', csrf=False, cors='*')
      #def get_overtimes(self):
      def hr_loan(self, db, login, password, base_location=None):
          request.session.logout()
          request.session.authenticate(db, login, password)
          loans_rec = request.env['hr.loan'].search([('employee_id.id','=',login), ('state', '=', "waiting_approval_2")])
          loans = []
          for rec in loans_rec:
              vals = {
                      'employee': rec.employee_id.name,
                      'employee_id': rec.employee_id.id,
                      'id': rec.id,
                      'amount':rec.loan_amount,
                      'payment date': rec.payment_date,
                      'balance': rec.balance_amount,
                      'duration': rec.installment,
                      'state': rec.state,
                      }
              loans.append(vals)
          data = {'status': 200, 'response': loans, 'message': 'Success'}
          return data
      @http.route('/refuse_hr_loan', type='json', auth='none', csrf=False, cors='*')
      #def get_overtimes(self):
      def refuse_hr_loan(self, db, login, password, base_location=None):
        request.session.logout()
        request.session.authenticate(db, login, password)
        if request.jsonrequest:
            vals = num
            newstate = {
                    "state":"refuse"
                    }
            leave = request.env['hr.loan'].search([('id', '=', vals)])
            finish = leave.write(newstate)
        print("Refuse Lona Is", finish)
        args = {'success': True, 'message': 'Success', 'ID':finish}
        return args
      @http.route('/accept_hr_loan', type='json', auth='none', csrf=False, cors='*')
      #def get_overtimes(self):
      def accept_hr_loan(self, db, login, password, base_location=None):
        request.session.logout()
        request.session.authenticate(db, login, password)
        if request.jsonrequest:
            vals = num
            newstate = {
                    "state":"approve"
                    }
            leave = request.env['hr.loan'].search([('id', '=', vals)])
            finish = leave.write(newstate)
        print("Accept Lona Is", finish)
        args = {'success': True, 'message': 'Success', 'ID':finish}
        return args
      @http.route('/get_finance_loans', type='json', auth='none', csrf=False, cors='*')
      #def get_overtimes(self):
      def finance_loan(self, db, login, password, base_location=None):
          request.session.logout()
          request.session.authenticate(db, login, password)
          loans_rec = request.env['hr.loan'].search([('employee_id.id','=',login), ('state', '=', "waiting_approval_1")])
          loans = []
          for rec in loans_rec:
              vals = {
                      'employee': rec.employee_id.name,
                      'employee_id':rec.employee_id.id,
                      'id': rec.id,
                      'amount':rec.loan_amount,
                      'payment date': rec.payment_date,
                      'balance': rec.balance_amount,
                      'duration': rec.installment,
                      'state': rec.state,
                      }
              loans.append(vals)
          data = {'status': 200, 'response': loans, 'message': 'Success'}
          return data
      @http.route('/accept_finance_loan', type='json', auth='none', csrf=False, cors='*')
      #def get_overtimes(self):
      def accept_finance_loan(self, db, login, password, base_location=None):
        request.session.logout()
        request.session.authenticate(db, login, password)
        if request.jsonrequest:
            vals = num
            newstate = {
                    "state":"waiting_approval_2"
                    }
            leave = request.env['hr.loan'].search([('id', '=', vals)])
            finish = leave.write(newstate)
        print("Accept Lona Is", finish)
        args = {'success': True, 'message': 'Success', 'ID':finish}
        return args
