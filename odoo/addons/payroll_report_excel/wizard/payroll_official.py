from odoo import fields, models
from odoo.tools.misc import get_lang
from datetime import date, datetime, time
from dateutil.relativedelta import relativedelta

class AccountBalanceReport(models.TransientModel):
    _name = 'report.payroll.official'
    _description = 'Report Payroll PDF'

    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.company)
    date_from = fields.Date(string='Start Date',required=True, default=(datetime.now()+relativedelta(months=-1)).strftime('%Y-%m-25'))
    date_to = fields.Date(string='End Date',required=True, default=(datetime.now()).strftime('%Y-%m-24'))
    emp_Financial_Manager = fields.Char(string="Emp Financial Manager")
    emp_HR_GRO_Manager = fields.Char(string="Emp HR GRO Manager")
    emp_Chief_Accountant = fields.Char(string="Emp Chief Accountant")
    salary_category = fields.Selection([
        ('iban', 'IBAN'),
        ('card', 'CARD'),
        ('cash', 'CASH'),
    ], 'Report Type',required=True, default='iban')


    def _build_contexts(self, data):
        result = {}
        result['date_from'] = data['form']['date_from'] or False
        result['date_to'] = data['form']['date_to'] or False
        result['company_id'] = data['form']['company_id'][0] or False
        result['salary_category'] = data['form']['salary_category'] or False
        result['emp_Chief_Accountant'] = data['form']['emp_Chief_Accountant'] or False
        result['emp_HR_GRO_Manager'] = data['form']['emp_HR_GRO_Manager'] or False
        result['emp_Financial_Manager'] = data['form']['emp_Financial_Manager'] or False

        return result

    def check_report(self):
        self.ensure_one()
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        self._set_employee_managers_name()
        data['form'] = self.read(['date_from', 'date_to','salary_category','company_id','emp_Financial_Manager','emp_HR_GRO_Manager','emp_Chief_Accountant'])[0]
        used_context = self._build_contexts(data)
        data['form']['used_context'] = dict(used_context, lang=get_lang(self.env).code)


        return self.with_context(discard_logo_check=True)._print_report(data)


    def _set_employee_managers_name(self):
        job_managers = self.env['hr.job'].search(
            ["|", "|", ("name", "=", "Chief Accountant"), ("name", "=", "HR & GRO Manager"),
             ("name", "=", "Financial Manager")])
        for job in job_managers:
            employee_managers = self.env['hr.employee'].search([("job_id", "=", job.id)])
            if job.name == "Chief Accountant":
                self.emp_Chief_Accountant = employee_managers.local_name
            if job.name == "HR & GRO Manager":
                self.emp_HR_GRO_Manager = employee_managers.local_name
            if job.name == "Financial Manager":
                self.emp_Financial_Manager = employee_managers.local_name


    def _print_report(self, data):
        records = self.env[data['model']].browse(data.get('ids', []))
        return self.env.ref(
            'payroll_report_excel.report_payroll_official').report_action(
            records, data=data)
