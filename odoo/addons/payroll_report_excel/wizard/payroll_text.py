from odoo import fields, models
from odoo.tools.misc import get_lang
from datetime import date, datetime, time
from datetime import timedelta
from dateutil.relativedelta import relativedelta

class AccountBalanceReport(models.TransientModel):
    _name = 'report.payroll.text'
    _description = 'Report Payroll Text'

    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.company)
    date_from = fields.Date(string='Start Date',required=True, default=(datetime.now()+relativedelta(months=-1)).strftime('%Y-%m-25'))
    date_to = fields.Date(string='End Date',required=True, default=(datetime.now()).strftime('%Y-%m-24'))
    salary_category = fields.Selection([
        ('iban', 'IBAN'),
        ('card', 'CARD'),
        # ('cash', 'CASH'),
    ], 'Report Type', required=True, default='iban')
    exchange_date_text = fields.Char(string="Exchange Date")
    currency_name = fields.Char(string="Currency Name")
    company_iban = fields.Char(string="Company IBAN")
    bank_number = fields.Char(string="Bank Number")

    def _build_contexts(self, data):
        result = {}
        result['date_from'] = data['form']['date_from'] or False
        result['date_to'] = data['form']['date_to'] or False
        result['salary_category'] = data['form']['salary_category'] or False
        result['company_id'] = data['form']['company_id'][0] or False
        return result

    def check_report(self):
        self.ensure_one()
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        self.set_exchange_date_text()
        self.currency_name = self.company_id[0].currency_id.name[:3]
        self.company_iban = self.company_id[0].iban
        self.bank_number = self.company_id[0].bank_number
        data['form'] = self.read(['date_from','salary_category','bank_number','currency_name','company_iban','exchange_date_text','date_to','company_id'])[0]
        used_context = self._build_contexts(data)
        data['form']['used_context'] = dict(used_context, lang=get_lang(self.env).code)
        return self.with_context(discard_logo_check=True)._print_report(data)

    def set_exchange_date_text(self):
        target_date = datetime.now() + timedelta(days=1)
        if date.today().weekday() == 4:
            target_date = target_date + timedelta(days=1)
        if date.today().weekday() == 3:
            target_date = target_date + timedelta(days=2)
        self.exchange_date_text = target_date.strftime('%Y%m%d')

    def _print_report(self, data):
        records = self.env[data['model']].browse(data.get('ids', []))
        taget_report=''
        if data['form']['salary_category'] == 'iban' :
            taget_report = 'payroll_report_excel.report_payroll_iban_text'
        if data['form']['salary_category'] == 'card' :
            taget_report = 'payroll_report_excel.report_payroll_card_text'

        return self.env.ref(taget_report).report_action(
            records, data=data)
