
import time

from odoo import api, models, _
from odoo.exceptions import UserError


class ReportTrialBalance(models.AbstractModel):
    _name = 'report.payroll_report_excel.report_payslip_iban_text'
    _description = 'Payslip Text Iban Report'

    @api.model
    def _get_report_values(self, docids, data=None):
        if not data.get('form'):
            raise UserError(
                _("Form content is missing, this report cannot be printed."))

        lines = self.env['hr.payslip'].search([("company_id", "=", data['form'].get('company_id')[0]),
                                               ("is_resignation", "=", False),
                                               ("employee_id.active", "=", True),
                                               ("emp_salary_category", "=", data['form'].get('salary_category')),
                                               ("date_from", "=", data['form'].get('date_from')),
                                               ("date_to", "=", data['form'].get('date_to')),
                                               ("Net_salary", ">", 0)])

        total_Net_salary = sum(lines.mapped('Net_salary'))
        total_count = len(lines)

        return {
            'data': data['form'],
            'lines': lines,
            'total_Net_salary': total_Net_salary,
            'total_count':total_count
        }


class ReportTrialBalance(models.AbstractModel):
    _name = 'report.payroll_report_excel.report_payslip_card_text'
    _description = 'Payslip Text  Card Report'

    @api.model
    def _get_report_values(self, docids, data=None):
        if not data.get('form'):
            raise UserError(
                _("Form content is missing, this report cannot be printed."))
        return {
            'data': data['form'],
            'lines': self.env['hr.payslip'].search([("company_id", "=", data['form'].get('company_id')[0]),
                                                    ("is_resignation", "=", False),
                                                    ("employee_id.active", "=", True),
                                                    ("emp_salary_category", "=", data['form'].get('salary_category')),
                                                    ("date_from", "=", data['form'].get('date_from')),
                                                    ("date_to", "=", data['form'].get('date_to')),
                                                    ("Net_salary", ">", 0)]),
        }
