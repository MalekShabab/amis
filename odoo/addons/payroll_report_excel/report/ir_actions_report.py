# -*- coding: utf-8 -*-

from odoo import models, api, _
from datetime import date, datetime, time
from datetime import timedelta

class PayrollIrActionsReport(models.Model):
    _inherit = 'ir.actions.report'

    def report_action(self, docids, data=None, config=True):
        exchange_date_text =''
        current_date_text = (datetime.now()).strftime('%Y%m%d')
        target_date = datetime.now() + timedelta(days=1)

        if date.today().weekday() == 4:
            target_date = target_date + timedelta(days=1)
        if date.today().weekday() == 3:
            target_date = target_date + timedelta(days=2)
        exchange_date_text = target_date.strftime('%Y%m%d')

        if self.report_name == 'payroll_report_excel.report_payslip_iban_text':
           self.name ='PCE_WPS_'+'96820576311_' + exchange_date_text + '_01_WPS.sign'
        if self.report_name == 'payroll_report_excel.report_payslip_card_text':
           self.name ='W_' + current_date_text + '_01_'+ exchange_date_text +'_w.sign'
        if self.report_name == 'payroll_report_excel.report_payslip_official':
           self.name ='Payroll_' + current_date_text + '_'+ exchange_date_text

        return super(PayrollIrActionsReport, self).report_action(docids,data,config)



