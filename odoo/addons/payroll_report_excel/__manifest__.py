{
    'name': 'payroll report in excel',
    'version': '1.0',
    'category': 'payroll',
    'sequence': 60,
    'summary': 'shows the payroll report in xlsx format',
    'description': "It shows payroll report in excel for given month",
    'author':'aswathy',
    'depends': ['base','hr', 'hr_payroll_community'],
    'data': [
        'wizard/payroll_report_wiz.xml',
        'wizard/payroll_official.xml',
        'wizard/payroll_text.xml',
        'report/report_payroll_official.xml',
        'report/report_payroll_text.xml',
        'report/report.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}