# -*- coding: utf-8 -*-

from odoo import models, fields, api

class RelatedTask(models.Model):
    _inherit = 'project.project'



    project_no = fields.Char(string="EZZ", required=False)
    test = fields.Char(string='Test', required=True)
    test2 = fields.Char(string='Test2', compute='ttt_res', store=True)
    n1 = fields.Float(string='N1', required=False,default=50)
    n2 = fields.Float(string='N2', required=False,default=lambda self:self.get_n2())
    res = fields.Float(string='Result',compute='_calc_res',store=True)

    def get_n2(self):
        return 4 * 4


    @api.depends('n1','n2')
    def _calc_res(self):
        for i in self:
            i.res=i.n1+i.n2

    @api.depends('test')
    def ttt_res(self):
        for rec in self:
            rec.test2=len(str(rec.test))




class RelatedTask(models.Model):
    _inherit = 'project.task'

    project_no2 = fields.Char(string="EZZ", related='project_id.name',store=True)




class RelatedTask(models.Model):
    _inherit = 'project.task'

    log_in = fields.Char(string="Log_in", related='user_id.email',store=True)
    log_in2 = fields.Char(string="Log_in2", related='user_id.phone',store=True)
