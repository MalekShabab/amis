from odoo import models, api, fields


class PayslipUnpaidLeave(models.Model):
    _inherit = 'hr.payslip'

#    unpaid_ids = fields.Many2many('hr.leave')

    @api.model
    def get_inputs(self, contracts, date_from, date_to,employee_id=False,leave_days_number=0,is_resignation=False):
        """
        function used for writing overtime record in payslip
        input tree.

        """
        res = super(PayslipUnPaid, self).get_inputs(contracts, date_to, date_from,employee_id)
        unpaid_type = self.env.ref('hr_holidays.hr_leave')
        contract = contracts
        unpaid_id = self.env['hr.leave'].search([('employee_id', '=', employee_id),
                                                      ('contract_id', '=', contract.id)])
        hrs_amount = unpaid_id.mapped('cash_hrs_amount')
       # day_amount = overtime_id.mapped('cash_day_amount')
        #cash_amount = sum(hrs_amount) + sum(day_amount)

        cash_amount = sum(hrs_amount)
        if unpaid_id:
            self.unpaid_ids = unpaid_id
            input_data = {
                'name': unpaid_type.name,
                'code':unpaid_type.code,
                'amount': cash_amount,
                'contract_id': contract.id,
            }
            res.append(input_data)
        return res

    def action_payslip_done(self):
        """
        function used for marking paid overtime
        request.

        """
        for recd in self.unpaid_ids:
   #         if recd.type == 'cash':
                recd.payslip_paid = True
        return super(PayslipUnPaid, self).action_payslip_done()

