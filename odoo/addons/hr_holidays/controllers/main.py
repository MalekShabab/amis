# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo.addons.mail.controllers.main import MailController
from odoo import http
from odoo.http import request
from datetime import timedelta, datetime


class HrHolidaysController(http.Controller):

    @http.route('/leave/validate', type='http', auth='user', methods=['GET'])
    def hr_holidays_request_validate(self, res_id, token):
        comparison, record, redirect = MailController._check_token_and_record_or_redirect('hr.leave', int(res_id), token)
        if comparison and record:
            try:
                record.action_approve()
            except Exception:
                return MailController._redirect_to_messaging()
        return redirect

    @http.route('/leave/refuse', type='http', auth='user', methods=['GET'])
    def hr_holidays_request_refuse(self, res_id, token):
        comparison, record, redirect = MailController._check_token_and_record_or_redirect('hr.leave', int(res_id), token)
        if comparison and record:
            try:
                record.action_refuse()
            except Exception:
                return MailController._redirect_to_messaging()
        return redirect

    @http.route('/allocation/validate', type='http', auth='user', methods=['GET'])
    def hr_holidays_allocation_validate(self, res_id, token):
        comparison, record, redirect = MailController._check_token_and_record_or_redirect('hr.leave.allocation', int(res_id), token)
        if comparison and record:
            try:
                record.action_approve()
            except Exception:
                return MailController._redirect_to_messaging()
        return redirect

    @http.route('/allocation/refuse', type='http', auth='user', methods=['GET'])
    def hr_holidays_allocation_refuse(self, res_id, token):
        comparison, record, redirect = MailController._check_token_and_record_or_redirect('hr.leave.allocation', int(res_id), token)
        if comparison and record:
            try:
                record.action_refuse()
            except Exception:
                return MailController._redirect_to_messaging()
        return redirect
    @http.route('/get_leaves', type='json', auth='none', csrf=False, cors='*')
   # def get_leaves(self):
    def leave(self, db, login, password, base_location=None):
          request.session.logout()
          request.session.authenticate(db, login, password)
          leaves_rec = request.env['hr.leave'].search([('employee_id.id','=',login),'|','|','|','|',('holiday_status_id','=',25),('holiday_status_id','=',32),('holiday_status_id','=',33),('holiday_status_id','=',34),('holiday_status_id','=',29)])
          leaves = []
          DateFormat = "%Y-%m-%d %H:%M:%S";

    
          for rec in leaves_rec:
              dfrom =  datetime.strptime(str(rec.date_from), DateFormat) + timedelta(hours=3)
              dto =  datetime.strptime(str(rec.date_to), DateFormat) + timedelta(hours=3)
              vals = {
                      'id': rec.id,
                      'name': rec.employee_id.name,
                      'type': rec.holiday_status_id.name,
                      'from': rec.request_date_from,
                      'to': rec.request_date_to,
                      'date_from':dfrom,
                      'date_to':dto,
                      'days': rec.number_of_days,
                      'state':rec.state,
                      'reason':rec.reason,
                      'device_info':rec.device_info,
                      'device_type':rec.device_type,
                      }
              leaves.append(vals)
          data = {'status': 200, 'response': leaves, 'message': 'Success'}
          return data
    @http.route('/create_leave', type='json', auth='none', csrf=False, cors='*')
    def create_leave(self, db, login, password, holiday_status_id, date_from, date_to, request_date_from, request_date_to, reason, device_info, device_type, documents, document_name):
        request.session.logout()
        request.session.authenticate(db, login, password)
        if request.jsonrequest:
         # print ("rec", rec)
         # if rec['state']:
              DateFormat = "%Y-%m-%d %H:%M:%S";
              check_from = datetime.strptime(date_from, DateFormat);
              ovfrom = check_from + timedelta(hours=-3);
              check_to = datetime.strptime(date_to, DateFormat);
              ovto = check_to + timedelta(hours=-3);
              vals = {
                      "holiday_status_id":holiday_status_id,
                      "date_from":ovfrom,
                      "date_to":ovto,
                      "request_date_from":request_date_from,
                      "request_date_to":request_date_to,
                      "state":"confirm",
                  #    "request_unit_half":"FALSE",
                      "request_unit_hours":"TRUE",
                      "reason":reason,
                      "device_info":device_info,
                      "device_type":device_type,
                      "documents":documents,
                      "document_name":document_name

              # 'date_from': '2020-09-07 08:08:05',

              # 'date_to': '2020-09-07 08:08:05'
            }
              new_leave = request.env['hr.leave'].sudo().create(vals)
        print("New  Leave Is", new_leave)
        args = {'success': True, 'message': 'Success', 'ID':new_leave.id}
        return args
    @http.route('/getoff', type='json', auth='none', csrf=False, cors='*')
    def off(self, db, login, password, base_location=None):
        request.session.logout()
        request.session.authenticate(db, login, password)
        leaves_rec = request.env['hr.leave'].search(['|','|',('holiday_status_id','=',24),('holiday_status_id','=',23),('holiday_status_id','=',35)])
        leaves = []
        DateFormat = "%Y-%m-%d %H:%M:%S";
        for rec in leaves_rec:
              dfrom =  datetime.strptime(str(rec.date_from), DateFormat) + timedelta(hours=3)
              dto =  datetime.strptime(str(rec.date_to), DateFormat) + timedelta(hours=3)
              vals = {
                      'id': rec.id,
                      'name': rec.employee_id.name,
                      'type': rec.holiday_status_id.name,
                      'from': dfrom,
                      'to': dto,
                      'days': rec.number_of_days,
                      'state':rec.state,
                      'reason':rec.reason,
                      'device_info':rec.device_info,
                      'device_type':rec.device_type
                      }
              leaves.append(vals)
        data = {'status': 200, 'response': leaves, 'message': 'Success'}
        return data

    @http.route('/create_off', type='json', auth='none', csrf=False, cors='*')
    def create_off(self, db, login, password, holiday_status_id, request_date_from, request_date_to):
        request.session.logout()
        request.session.authenticate(db, login, password)
        if request.jsonrequest:
         # print ("rec", rec)
         # if rec['state']:
              vals = {
                      "holiday_status_id":holiday_status_id,
                      "date_from":request_date_from,
                      "date_to":request_date_to,
                      "state":"confirm",
                   #   "request_unit_half":"FALSE",
                      "request_unit_hours":"FALSE"

              # 'date_from': '2020-09-07 08:08:05',

              # 'date_to': '2020-09-07 08:08:05'
            }
              new_off = request.env['hr.leave'].sudo().create(vals)
        print("New  Off Is", new_off)
        args = {'success': True, 'message': 'Success', 'ID':new_off.id}
        return args


    @http.route('/delete_off', type='json', auth='none', csrf=False, cors='*')
    def unlink_off(self, db, login, password, num):
        request.session.logout()
        request.session.authenticate(db, login, password)
        if request.jsonrequest:
         # print ("rec", rec)
         # if rec['state']:
              vals = num
                     # "id": num
                    
              # 'date_from': '2020-09-07 08:08:05',
              # 'date_to': '2020-09-07 08:08:05'
          #  }
              #del_off = request.env['hr.leave'].sudo().unlink(vals)
        #      request.env['hr.leave'].unlink(vals)



              leave = request.env['hr.leave'].search([('id', '=', num)])
              test = leave.unlink()
        print("Deleted  Off Is", test)
        args = {'success': True, 'message': 'Success', 'ID':test}
        return args
    @http.route('/get_balance', type='json', auth='none', csrf=False, cors='*')
    def balance(self, db, login, password, base_location=None):
        request.session.logout()
        request.session.authenticate(db, login, password)
        leaves_rec = request.env['hr.leave.allocation'].search([('employee_id.id', '=', login)])
        leaves = []
        for rec in leaves_rec:
              vals = {
                    #  'id': rec.id,
                      'name': rec.employee_id.name,
                      'type': rec.holiday_status_id.id,
                      'days': rec.number_of_days
                      }
              leaves.append(vals)
        data = {'status': 200, 'response': leaves, 'message': 'Success'}
        return data
    @http.route('/vac', type='json', auth='none', csrf=False, cors='*')
   # def get_leaves(self):
    def vac(self, db, login, password, base_location=None):
        request.session.logout()
        request.session.authenticate(db, login, password)
        leaves_rec = request.env['hr.leave'].search([('employee_id.id','=',login),'|','|',('holiday_status_id','=',24),('holiday_status_id','=',23),('holiday_status_id','=',35)])
        leaves = []
        DateFormat = "%Y-%m-%d %H:%M:%S";
        for rec in leaves_rec:
              dfrom =  datetime.strptime(str(rec.date_from), DateFormat) + timedelta(hours=3)
              dto =  datetime.strptime(str(rec.date_to), DateFormat) + timedelta(hours=3)
              vals = {
                      'id': rec.id,
                      'name': rec.employee_id.name,
                      'type': rec.holiday_status_id.name,
                      'from': dfrom,
                      'to': dto,
                      'days': rec.number_of_days,
                      'state':rec.state,
                      }
              leaves.append(vals)
        data = {'status': 200, 'response': leaves, 'message': 'Success'}
        return data
    @http.route('/get_holiday_manager', type='json', auth='none', csrf=False, cors='*')
    def off(self, db, login, password, base_location=None):
        request.session.logout()
        request.session.authenticate(db, login, password)
        flag = request.env['res.users'].search([('id', '=', login)]).has_group('hr_holidays.group_hr_holidays_manager')
        return flag
    @http.route('/get_managedoff', type='json', auth='none', csrf=False, cors='*')
    def managedoff(self, db, login, password, base_location=None):
        request.session.logout()
        request.session.authenticate(db, login, password)
        leaves_rec = request.env['hr.leave'].search([('employee_id.leave_manager_id', '=', login), ('state', '=', "confirm")])
        leaves = []
        for rec in leaves_rec:
              vals = {
                      'id': rec.id,
                      'name': rec.employee_id.name,
                      'employee_id': rec.employee_id.id,
                      'type': rec.holiday_status_id.name,
                      'from': rec.date_from,
                      'to': rec.date_to,
                      'days': rec.number_of_days,
                      'reason':rec.reason,
                      'state':rec.state
                      }
              leaves.append(vals)
        data = {'status': 200, 'response': leaves, 'message': 'Success'}
        return data
    @http.route('/accept_off', type='json', auth='none', csrf=False, cors='*')
    def accept_off(self, db, login, password, num):
        request.session.logout()
        request.session.authenticate(db, login, password)
        if request.jsonrequest:
            vals = num
            newstate = {
                    "state":"validate1"
                    }
                     # "id": num
              # 'date_from': '2020-09-07 08:08:05',
              # 'date_to': '2020-09-07 08:08:05'
          #  }
              #del_off = request.env['hr.leave'].sudo().unlink(vals)
        #      request.env['hr.leave'].unlink(vals)
            leave = request.env['hr.leave'].search([('id', '=', vals)])
            write = leave.write(newstate)
        print("Accept  Off By Manager", write)
        args = {'success': True, 'message': 'Success', 'ID':write}
        return args
    @http.route('/refuse_leaves', type='json', auth='none', csrf=False, cors='*')
    def refuse_leaves(self, db, login, password, num):
        request.session.logout()
        request.session.authenticate(db, login, password)
        if request.jsonrequest:
            vals = num
            newstate = {
                    "state":"refuse"
                    }
                     # "id": num
              # 'date_from': '2020-09-07 08:08:05',
              # 'date_to': '2020-09-07 08:08:05'
          #  }
              #del_off = request.env['hr.leave'].sudo().unlink(vals)
        #      request.env['hr.leave'].unlink(vals)
            leave = request.env['hr.leave'].search([('id', '=', vals)])
            write = leave.write(newstate)
        print("Deleted  Off Is", write)
        args = {'success': True, 'message': 'Success', 'ID':write}
        return args
    @http.route('/get_hroff', type='json', auth='none', csrf=False, cors='*')
    def hroff(self, db, login, password, base_location=None):
        request.session.logout()
        request.session.authenticate(db, login, password)
        leaves_rec = request.env['hr.leave'].search([('state', '=', "validate1")])
        leaves = []
        for rec in leaves_rec:
              vals = {
                      'id': rec.id,
                      'name': rec.employee_id.name,
                      'employee_id': rec.employee_id.id,
                      'type': rec.holiday_status_id.name,
                      'from': rec.request_date_from,
                      'to': rec.request_date_to,
                      'days': rec.number_of_days,
                      'state':rec.state,
                      'reason':rec.reason,
                      'device_info':rec.state,
                      'device_type':rec.device_type,
                      }
              leaves.append(vals)
        data = {'status': 200, 'response': leaves, 'message': 'Success'}
        return data
    @http.route('/accept_hr_off', type='json', auth='none', csrf=False, cors='*')
    def accept_hr_off(self, db, login, password, num):
        request.session.logout()
        request.session.authenticate(db, login, password)
        if request.jsonrequest:
            vals = num
            newstate = {
                    "state":"validate"
                    }
                     # "id": num
              # 'date_from': '2020-09-07 08:08:05',
              # 'date_to': '2020-09-07 08:08:05'
          #  }
              #del_off = request.env['hr.leave'].sudo().unlink(vals)
        #      request.env['hr.leave'].unlink(vals)
            leave = request.env['hr.leave'].search([('id', '=', vals)])
            write = leave.write(newstate)
        print("Accept  Off By HR", write)
        args = {'success': True, 'message': 'Success', 'ID':write}
        return args
