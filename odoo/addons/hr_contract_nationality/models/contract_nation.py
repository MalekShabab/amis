# -*- coding: utf-8 -*-

from odoo import models, fields, api
from  datetime import datetime
from dateutil.relativedelta import relativedelta



class ContractNation(models.Model):
    _name = 'hr.contract.nation'
    _description = 'Nationality Type'
    _order = 'sequence, id'

    name = fields.Char(string='Contract Nation', required=True, help="Name")
    sequence = fields.Integer(help="Gives the sequence when displaying a list of .", default=10)

class employeelevel(models.Model):
    _name = 'hr.contract.level'
    _description = 'Employee Level'
    _order = 'sequence, id'

    level = fields.Char(string='Employee Leve', required=True, help="level")
    grade = fields.Char(string='Employee Grade', required=True, help="grade")
    sequence = fields.Integer(help="Gives the sequence when displaying a list of .", default=10)


class ContractInherit(models.Model):
    _inherit = 'hr.contract'

    nation_id = fields.Many2one('hr.contract.nation', string="Employee Nationality",
                              required=True, help="Employee Nationality",
                              default=lambda self: self.env['hr.contract.nation'].search([], limit=1))
    country_id = fields.Many2one('res.country', 'name', help="Country", required=True)
    birth_date = fields.Date(string="Date Of Birth")
    age = fields.Char(string="Age", compute="_compute_age")
    #resident = fields.Boolean(default=True)
    resident = fields.Selection([
        ('true', 'True'),
        ('false', 'False')
    ], string='Resident', groups="hr.group_hr_user", default='true', tracking=True)

    #arrival_date = fields.Date(string="Arrival Date", help="Arrival Date", required=True)
    arrival_date = fields.Date(string='arrival_date', default=datetime.today(), help="Arrival Date", required=True)
    level_ids = fields.Many2one('hr.contract.level','level' ,required=True)
    #date_start = fields.Date(string="Arrival Date", help="Arrival Date", required=True, compute="_get_end_date")
    #date_start = fields.Date(string="Start Date", default=datetime.today(), compute="_get_date_start")
    date_start = fields.Date(string="Start Date")
    date_current = fields.Date(string="Current Date", default=datetime.today())

    @api.depends('birth_date')
    def _compute_age(self):
        for emp in self:
            age = relativedelta(datetime.now().date(), fields.Datetime.from_string(emp.birth_date)).years
            emp.age = str(age) + " Years"


   # @api.depends('resident', 'arrival_date')
   # def _get_end_date(self):
       # for r in self:
          #  if r.resident == "True":
           #    r.date_start = r.arrival_date
           # else
             #   r.date_start = r.arrival_date


   # @api.depends('resident', 'arrival_date')
   # def _get_end_date(self):
     #   for r in self:
        #       r.date_start = r.arrival_date
