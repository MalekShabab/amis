# -*- coding: utf-8 -*-
# Part of Softhealer Technologies.
{
    "name": "Account Statement | Customer Account Statement | Customer Overdue Statement | Vendor Bank Statement | Vendor Bank Overdue Statement",
    "author": "Softhealer Technologies",
    "website": "https://www.softhealer.com",
    "support": "support@softhealer.com",
    "version": "13.0.9",
    "category": "Accounting",
    "summary": "Bank Statement, Accounts Statement, Customer Bank Statement, Supplier Statement, Vendor Statement, Overdue Statement, Account Statement Report, Partner Statement of Account, Bank Account Detail Report, Bank Details Odoo",
    "description": """This module allows customers or vendors to see statements as well as overdue statement details. You can send statements by email to the customers and vendors. You can also see customers/vendors mail log history with statements and overdue statements. You can also send statements automatically weekly, monthly & daily, or you can send statements using cron job also. You can filter statements by dates, statements & overdue statements. You can group by statements by the statement type, mail sent status & customers/vendors. You can print statements and overdue statements.""",
    "depends": [
        'account'
    ],
    "data": [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/user.xml',
        'views/mail_history.xml',
        'views/res_config_setting.xml',
        'views/res_partner.xml',
        'report/customer_statement_report.xml',
        'report/customer_due_statement_report.xml',
        'report/vendor_statement_report.xml',
        'report/vendor_due_statement_report.xml',
        'report/customer_filter_statement_report.xml',
        'report/vender_filtered_statement_report.xml',
        'report/paperformat.xml',
        'report/summary_statement_report.xml',
        'data/email_data.xml',
        'data/statement_cron.xml',
        'views/assets.xml',
        'views/customer_statement_portal_templates.xml',
        'views/vendor_statement_portal_templates.xml',
        'wizard/mail_compose_view.xml',
    ],
    "images": ["static/description/background.png", ],
    "license": "OPL-1",
    "installable": True,
    "auto_install": False,
    "application": True,
    "price": "80",
    "currency": "EUR"
}
