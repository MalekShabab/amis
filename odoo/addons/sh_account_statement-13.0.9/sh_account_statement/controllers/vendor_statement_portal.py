# -*- coding: utf-8 -*-
# Part of Softhealer Technologies.

from odoo import http
from odoo.http import request
from odoo.addons.portal.controllers.portal import CustomerPortal
import json
import werkzeug.utils


class VendorStatementPortal(CustomerPortal):

    def _prepare_portal_layout_values(self):
        values = super(VendorStatementPortal,
                       self)._prepare_portal_layout_values()
        values.update({
            'page_name': 'sh_vendor_statement_portal',
            'default_url': '/my/vendor_statements',
        })
        return values

    @http.route(['/my/vendor_statements', '/my/vendor_statements/page/<int:page>'], type='http', auth="user", website=True)
    def portal_my_vendor_statements(self, **kw):
        values = self._prepare_portal_layout_values()
        partner_id = request.env.user.partner_id
        vendor_statement_ids = partner_id.sh_vendor_statement_ids
        vendor_overdue_statement_ids = partner_id.sh_vendor_due_statement_ids
        values.update({
            'vendor_statement_ids': vendor_statement_ids,
            'vendor_overdue_statement_ids': vendor_overdue_statement_ids,
            'page_name': 'sh_vendor_statement_portal',
            'default_url': '/my/vendor_statements',
        })
        return request.render("sh_account_statement.sh_vendor_statement_portal", values)

    @http.route(['/my/vendor_statements/send'], type='http', auth="user", website=False,csrf=False)
    def vendor_statement_send(self, **post):
        dic = {}
        if post.get('vendor_send_statement') == 'true':
            request.env.user.partner_id.action_send_vendor_statement()
            dic.update({
                'msg':'Statement Sent Successfully.....'
                })
        if post.get('vendor_send_overdue_statement') == 'true':
            request.env.user.partner_id.action_send_vendor_due_statement()
            dic.update({
                'msg':'Statement Sent Successfully.....'
                })
        return json.dumps(dic)
    
    @http.route(['/my/vendor_statements/xls'], type='http', auth="user", website=False,csrf=False)
    def vendor_statement_xls(self, **post):
        res =  request.env.user.partner_id.action_print_vendor_statement_xls()
        return werkzeug.utils.redirect(res.get('url'))
    
    @http.route(['/my/vendor_statements_due/xls'], type='http', auth="user", website=False,csrf=False)
    def vendor_statement_xls_due(self, **post):
        res =  request.env.user.partner_id.action_print_vendor_due_statement_xls()
        return werkzeug.utils.redirect(res.get('url'))
