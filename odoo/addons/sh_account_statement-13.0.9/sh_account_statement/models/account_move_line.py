from odoo import models, fields, api, _

class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    sh_balance = fields.Monetary('Balance',compute = "compute_sh_balance",currency_field='company_currency_id')

    def compute_sh_balance(self):
        for rec in self:
            rec.sh_balance = 0.0
            rec.sh_balance = rec.debit - rec.credit
        